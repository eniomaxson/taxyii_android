package br.taxyii.library.activity;

import java.io.File;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;
import br.taxyii.library.R;
import br.taxyii.library.faces.ITransacao;
import br.taxyii.library.model.CidadeModel;
import br.taxyii.library.model.UsuarioModel;
import br.taxyii.library.task.BuscarCidadeTask;
import br.taxyii.library.task.DefaultTask;
import br.taxyii.library.utils.AndroidUtils;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.ActionMode;

public class CriarUsuarioActivity extends SherlockActivity implements
		ITransacao {

	protected ImageView foto;
	protected UsuarioModel usuario;
	protected EditText cidade;
	protected EditText nome;
	protected EditText sobrenome;
	protected EditText email;
	protected EditText senha;
	protected EditText cep;
	protected Spinner sexo;
	protected EditText celular;
	protected Button btnCadastrar;
	protected ArrayList<String> sexos;
	protected BuscarCidadeTask cidadeTask;
	protected DefaultTask criarUsuarioTask;
	public boolean validado = true;
	protected String response;
	protected ActionMode mMode;
	protected String imgDirName = "taxyii";
	protected String imgName = "user.jpg";
	protected int REQUEST_CAMERA = 1;
	protected int REQUEST_GALERIA = 2;
	protected int SALVAR = 1;
	public File file;
	public boolean conectado;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
	}

	public void setListenerCep() {
		cep.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus) {
					if (AndroidUtils.checkConnection(CriarUsuarioActivity.this)) {
						if (!cep.getText().toString().equals("")) {
							cidadeTask = new BuscarCidadeTask();
							cidadeTask.activity = CriarUsuarioActivity.this;
							cidadeTask.execute(cep.getText().toString());
							cidadeTask.cidade = cidade;
						}
					} else {
						AndroidUtils.showSimpleToast(CriarUsuarioActivity.this,
								R.string.error_conexao, Toast.LENGTH_SHORT);
					}
				} else {
					cep.setText("");
				}
			}
		});
	}

	public void createUsuario() {

		setInputErros();

		conectado = AndroidUtils.checkConnection(this);

		if (validado && conectado) {
			usuario = new UsuarioModel();
			usuario.setCelular(celular.getText().toString());
			usuario.setCidade(new CidadeModel(cidade.getText().toString()));
			usuario.setNome(nome.getText().toString());
			usuario.setSobrenome(sobrenome.getText().toString());
			usuario.setEmail(email.getText().toString());
			usuario.setSenha(senha.getText().toString());
			usuario.setSexo(sexo.getSelectedItem().toString());
			// usuario.setFoto(null);

			criarUsuarioTask = new DefaultTask(this);

			criarUsuarioTask.execute();
		}
		validado = true;

	};

	public void setInputErros() {

		if (cidade.getText().toString().equals("")) {
			cep.setError(getString(R.string.errorCidade));
			validado = false;
		}

		if (!nome.getText().toString()
				.matches("[^0-9][a-zA-ZáéíóãõêâÁÉÍÓÃÕÊÃ ]*")) {
			nome.setError(getString(R.string.errorNome));
			validado = false;
		}

		if (!sobrenome.getText().toString()
				.matches("[^0-9][a-zA-ZáéíóãõêâÁÉÍÓÃÕÊÃ ]*")) {
			sobrenome.setError(getString(R.string.errorSobrenome));
			validado = false;
		}
		String regex = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

		if (!email.getText().toString().matches(regex)) {
			email.setError(getString(R.string.errorEmail));
			validado = false;
		}
		if (!celular.getText().toString().matches("[0-9]{10}[0-9]?")) {
			celular.setError(getString(R.string.errorCelular));
			validado = false;
		}

		if (!(senha.length() >= 6)) {
			senha.setError(getString(R.string.errorSenha));
			validado = false;
		}
		if (sexo.getSelectedItem().toString().length() == 0) {
			validado = false;
		}
	}

	@Override
	public void executar() throws Exception {
		response = usuario.cadastrar(this);
	}

	@Override
	public void atualizarView() {
		if (response != null) {
			if (response.contains("error")) {
				showErrorServer(response);
			} else {
				AndroidUtils.showSimpleToast(this,
						R.string.msgUsuarioCadastroSucesso, Toast.LENGTH_SHORT);
				finish();
			}
		} else {
			AndroidUtils.showSimpleToast(this, R.string.error_conexao,
					Toast.LENGTH_SHORT);
		}
	}

	public void showErrorServer(String jsonError) {
		try {
			JSONObject error = new JSONObject(jsonError);
			
			error = error.getJSONObject("error");
			
			if (error.has("email")) {
				AndroidUtils.showSimpleToast(CriarUsuarioActivity.this,
						R.string.error_email_cadastrado, Toast.LENGTH_SHORT);
			}
		} catch (JSONException e) {

		}
	}
}
