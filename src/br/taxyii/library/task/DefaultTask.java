package br.taxyii.library.task;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import br.taxyii.library.faces.ITransacao;
import br.taxyii.library.utils.AndroidUtils;


public class DefaultTask extends AsyncTask<Void, Void, Void> {
	private ITransacao transacao;
	private Activity activity;
	private ProgressDialog p;
	private String message;
	private String title;
	private int icon = 0;
	
	public DefaultTask(ITransacao transacao) {
		this.transacao = transacao;
	}

	public DefaultTask(Activity activity, ITransacao transacao) {
		this.activity = activity;
		this.transacao = transacao;
	}

	public DefaultTask(Activity activity, ITransacao transacao, String title,String message, int icon) {
		this.activity = activity;
		this.transacao = transacao;
		this.title = title;
		this.message = message;
		this.icon = icon;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		if (title != null && message != null) {
			p = AndroidUtils.showProgressDialog(activity, title, message, icon);
			p.show();
		}
	}

	@Override
	protected Void doInBackground(Void... params) {
		try {
			transacao.executar();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);

		if (p != null) {
			p.dismiss();
		}
		transacao.atualizarView();
	}
}
