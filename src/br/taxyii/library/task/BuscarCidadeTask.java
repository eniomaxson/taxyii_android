package br.taxyii.library.task;

import java.util.HashMap;

import org.jsoup.select.Elements;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.EditText;
import android.widget.Toast;
import br.taxyii.library.R;
import br.taxyii.library.utils.AndroidUtils;
import br.taxyii.library.utils.HttpClientUtils;


public class BuscarCidadeTask extends AsyncTask<String, Void, Elements> {
	public Activity activity;
	public String url = "http://m.correios.com.br/movel/buscaCepConfirma.do";
	public HttpClientUtils http;
	public HashMap<String, String> map;
	public Elements retorno;
	public EditText cidade;
	public ProgressDialog p;

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		p = AndroidUtils.showProgressDialog(activity,"Aguarde...", "Localizando cidade", 0);
		p.show();
	}

	@Override
	protected Elements doInBackground(String... params) {
		if (params[0] != null && !(params[0].equals(""))) {
			map = new HashMap<String, String>();
			map.put("cepEntrada", params[0]);
			map.put("metodo", "buscarCep");
			map.put("tipoCep", "");
			map.put("cepTemp", "");
			http = new HttpClientUtils(url);
			try {
				retorno = http.executaJsonp(map);
				return retorno;
			} catch (Exception e) {
				return null;
			}
		}
		return null;
	}

	@Override
	protected void onPostExecute(Elements result) {
		super.onPostExecute(result);
		if (result != null) {
			if (result.size() > 0) {
				cidade.setText(result.get(2).text());
			} else {
				AndroidUtils.showSimpleToast(activity, R.string.cidade_nao_encontrada, Toast.LENGTH_SHORT);
			}
		} else {
			AndroidUtils.showSimpleToast(activity, R.string.error_conexao, Toast.LENGTH_SHORT);
		}
		p.dismiss();
	}
}
