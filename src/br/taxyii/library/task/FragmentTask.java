package br.taxyii.library.task;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;
import br.taxyii.library.R;
import br.taxyii.library.faces.ITransacao;
import br.taxyii.library.fragment.TaxyiiFragment;
import br.taxyii.library.utils.AndroidUtils;

public class FragmentTask extends AsyncTask<Void, Void, Boolean> {
	private static final String TAG = "taxyii";
	private final Context context;
	private final TaxyiiFragment fragment;
	private final ITransacao transacao;
	private Exception exceptionErro;

	public FragmentTask(TaxyiiFragment fragment, ITransacao transacao) {
		this.context = fragment.getActivity();
		this.fragment = fragment;
		this.transacao = transacao;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		try {
			showProgress();
		} catch (Exception e) {
			Log.e(TAG, e.getMessage(), e);
		}
	}

	@Override
	protected Boolean doInBackground(Void... params) {
		try {
			transacao.executar();
		} catch (Exception e) {
			Log.e(TAG, e.getMessage(), e);
			// Salva o erro e retorna false
			this.exceptionErro = e;
			return false;
		} finally {
			try {
				Activity ac = fragment.getActivity();
				if (ac != null) {
					ac.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							hideProgress();
						}
					});
				}
			} catch (Exception e) {
				Log.e(TAG, e.getMessage(), e);
			}
		}
		// OK
		return true;
	}

	@Override
	protected void onPostExecute(Boolean ok) {
		if (ok) {
			transacao.atualizarView();
		} else {
			AndroidUtils.showSimpleToast(fragment.getActivity(),R.string.error_conexao, Toast.LENGTH_LONG);
		}
	}

	// Exibe o ProgressBar
	private void showProgress() {
		fragment.showProgress(true);
	}

	// Desliga o ProgressBar
	private void hideProgress() {
		fragment.showProgress(false);
	}
}