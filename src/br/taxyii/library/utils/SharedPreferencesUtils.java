package br.taxyii.library.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferencesUtils {

	public final static String PASSAGEIRO_PREFERENCES = "PASSAGEIRO";
	public final static String GCM_PREFERENCES = "GCM";
	public final static String GCM_REGISTRATION_ID = "GCMID";
	public final static String GCM_ENVIADO = "ENVIADO";
	public final static String APP_VERSION = "APPVERSION";
	public final static String STATUS_PREFERENCES = "STATUS";
	public final static String STATUS_TAXI = "STATUS_TAXI";
	public final static String STATUS_TAXISTA= "STATUS_TAXISTA";
	public final static String CORRIDA_PREFERENCES = "CORRIDA_ATUAL";
	public final static String CORRIDA_ATUAL = "CORRIDA_ATUAL";
	
	private SharedPreferences preferences;

	public SharedPreferencesUtils(Activity activity, String nome_arquivo) {
		this.preferences = activity.getSharedPreferences(nome_arquivo, 0);
	}

	public SharedPreferencesUtils(Context context, String nome_arquivo) {
		this.preferences = context.getSharedPreferences(nome_arquivo, 0);
	}

	/*
	 * Método responsavel por salvar uma preferencia
	 */
	public void setPreferences(String key, String value) {
		SharedPreferences.Editor editor = preferences.edit();
		editor.putString(key, value);
		editor.commit();
	}

	/*
	 * Método responsavel por recuperar uma preferencia
	 */
	public String getPreferences(String key, String defalt_value) {
		String value = preferences.getString(key, defalt_value);
		return value;
	}

	/*
	 * Método responsavel por deletar uma preferencia
	 */
	public void removePreferences(String key) {
		SharedPreferences.Editor edit = preferences.edit();
		edit.remove(key);
		edit.commit();
	}
}
