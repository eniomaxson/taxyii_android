package br.taxyii.library.utils;

import java.io.IOException;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;

public class GcmUtils {

	/*
	 * Variaveis para registro no gcm
	 */
	public static final String PROPERTY_REG_ID = SharedPreferencesUtils.GCM_REGISTRATION_ID;
	private static final String PROPERTY_APP_VERSION = SharedPreferencesUtils.APP_VERSION;

	/**
	 * Substitute you own sender ID here. This is the project number you got
	 * from the API Console, as described in "Getting Started."
	 */
	public static String SENDER_ID_MOTORISTA = "49105500219";
	public static String SENDER_ID_PASSAGEIRO = "370978399959";
	/**
	 * Tag used on log messages.
	 */

	private GoogleCloudMessaging gcm;

	private Context context;
	private String regid;

	public GcmUtils(Context context) {
		this.context = context;
	}

	public void register(String SENDER_ID) {
		try {
			if (gcm == null) {
				gcm = GoogleCloudMessaging.getInstance(context);
			}

			if (getRegistrationId() == null) {
				setRegid(gcm.register(SENDER_ID));
				storeRegistrationId(getRegid());
				Log.i("GCM_ID", regid!=null? regid:"");
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public String getRegistrationId() {
		final SharedPreferences prefs = getGCMPreferences(context);
		String registrationId = prefs.getString(
				SharedPreferencesUtils.GCM_REGISTRATION_ID, null);

		int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION,
				Integer.MIN_VALUE);
		int currentVersion = getAppVersion(context);

		if (registeredVersion != currentVersion) {
			registrationId = null;
		}
		return registrationId;
	}

	private SharedPreferences getGCMPreferences(Context context) {
		return context.getSharedPreferences(
				SharedPreferencesUtils.GCM_PREFERENCES, Context.MODE_PRIVATE);
	}

	private void storeRegistrationId(String regId) {
		final SharedPreferences prefs = getGCMPreferences(context);
		int appVersion = getAppVersion(context);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(PROPERTY_REG_ID, regId);
		editor.putInt(PROPERTY_APP_VERSION, appVersion);
		editor.putString(SharedPreferencesUtils.GCM_ENVIADO, "N");
		editor.commit();
	}

	private static int getAppVersion(Context context) {
		try {
			PackageInfo packageInfo = context.getPackageManager()
					.getPackageInfo(context.getPackageName(), 0);
			return packageInfo.versionCode;

		} catch (NameNotFoundException e) {
			// should never happen
			throw new RuntimeException("Could not get package name: " + e);
		}
	}

	public String getRegid() {
		return regid;
	}

	public void setRegid(String regid) {
		this.regid = regid;
	}
}