package br.taxyii.library.utils;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class HttpClientUtils {

	public static final int HTTP_TIMEOUT = 13000;
	private static HttpClient httpClient;
	private String url;

	public HttpClientUtils(String url) {
		this.url = url;
	}

	/*
	 * Metodo retorna um httpClient para realizar uma conexão tipo get ou post
	 * parametriza alguns valores
	 */

	private HttpClient getHttpClient() {
		if (httpClient == null) {
			httpClient = new DefaultHttpClient();
			final HttpParams httpParamns = httpClient.getParams();
			HttpConnectionParams
					.setConnectionTimeout(httpParamns, HTTP_TIMEOUT);
			HttpConnectionParams.setSoTimeout(httpParamns, HTTP_TIMEOUT);
			ConnManagerParams.setTimeout(httpParamns, HTTP_TIMEOUT);
		}
		return httpClient;
	}

	// Executa uma requisição post e retorna a uma string json

	public String requisicaoPostJson(String parametrosPost) throws Exception {
		BufferedReader bufferedReader = null;
		try {
			HttpClient client = getHttpClient();

			HttpPost httpPost = new HttpPost(url);

			httpPost.setHeader("Accept", "application/json");
			httpPost.setHeader("Content-type", "application/json");

			httpPost.setEntity(new StringEntity(parametrosPost));

			HttpResponse httpResponse = client.execute(httpPost);

			bufferedReader = new BufferedReader(new InputStreamReader(
					httpResponse.getEntity().getContent()));

			StringBuffer stringBuffer = new StringBuffer("");

			String line = "";

			String LS = System.getProperty("line.separator"); // \s

			while ((line = bufferedReader.readLine()) != null) {
				stringBuffer.append(line + LS);
			}

			bufferedReader.close();

			String resultado = stringBuffer.toString();

			return resultado;
		} finally {
			if (bufferedReader != null) {
				try {
					bufferedReader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}

	// Executa uma requisição get e retorna uma string em formato json

	public String requisicaoGetJson() throws Exception {

		BufferedReader bufferedReader = null;
		try {
			HttpClient client = getHttpClient();

			HttpGet httpGet = new HttpGet(url);

			httpGet.setURI(new URI(url));

			HttpResponse httpResponse = client.execute(httpGet);

			bufferedReader = new BufferedReader(new InputStreamReader(
					httpResponse.getEntity().getContent()));

			StringBuffer stringBuffer = new StringBuffer("");

			String line = "";

			String LS = System.getProperty("line.separator"); // \s

			while ((line = bufferedReader.readLine()) != null) {
				stringBuffer.append(line + LS);
			}

			bufferedReader.close();

			String resultado = stringBuffer.toString();

			return resultado;
		} finally {
			if (bufferedReader != null) {
				try {
					bufferedReader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}

	/*
	 * Metodo de requisição comum via Post como dados de um formulário web
	 */

	public String requisicaoPost(Map<String, Object> map) {
		try {
			HttpClient httpclient = getHttpClient();
			HttpPost httpPost = new HttpPost(url);
			// cria os parâmetros
			List<NameValuePair> params = getParams(map);
			// seta os parametros para enviar
			httpPost.setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));

			HttpResponse response = httpclient.execute(httpPost);

			HttpEntity entity = response.getEntity();

			if (entity != null) {
				InputStream in = entity.getContent();
				String texto = readString(in);
				return texto;
			}
		} catch (Exception e) {
			return null;
		}
		return null;
	}

	/*
	 * Seta os paramentros de uma requisição post
	 */
	private List<NameValuePair> getParams(Map<String, Object> map)
			throws IOException {
		if (map == null || map.size() == 0) {
			return null;
		}

		List<NameValuePair> params = new ArrayList<NameValuePair>();

		Iterator<String> e = map.keySet().iterator();
		while (e.hasNext()) {
			String name = e.next();
			Object value = map.get(name);
			params.add(new BasicNameValuePair(name, String.valueOf(value)));
		}

		return params;
	}

	/*
	 * Faz leitura de um input estream e retorna uma string
	 */
	private String readString(InputStream in) throws IOException {
		byte[] bytes = readBytes(in);
		String texto = new String(bytes);
		return texto;
	}

	/*
	 * Lee um inputstream e retorna os bytes correspondentes
	 */
	private byte[] readBytes(InputStream in) throws IOException {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try {
			byte[] buf = new byte[1024];
			int len;
			while ((len = in.read(buf)) > 0) {
				bos.write(buf, 0, len);
			}

			byte[] bytes = bos.toByteArray();
			return bytes;
		} finally {
			bos.close();
		}
	}

	/*
	 * Metodo usado para buscar um endereço por um cep
	 */
	public Elements executaJsonp(HashMap<String, String> query) {
		try {
			Document doc = Jsoup.connect(url).data(query).post();
			Elements elemetos = doc.select(".respostadestaque");
			return elemetos;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}