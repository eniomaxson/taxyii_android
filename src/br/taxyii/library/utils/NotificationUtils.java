package br.taxyii.library.utils;


import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.Notification.Builder;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import br.taxyii.library.R;


public class NotificationUtils {
	
	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	public static void create(Context context, String tickerText, String title,
			String message, int icon, int id, Intent intent) {

		// PendingIntent para executar a intent ao selecionar a notificao
		PendingIntent p = PendingIntent.getActivity(context, 0, intent, 0);

		Notification n = null;

		int apiLevel = Build.VERSION.SDK_INT;
		if (apiLevel >= 11) {
			Builder builder = new Notification.Builder(context);
			builder.setContentTitle(tickerText);
			builder.setContentText(message);
			builder.setSmallIcon(icon);
			
			builder.setContentIntent(p);

			if (apiLevel >= 17) {
				// Android 4.2
				n = builder.build();
			} else {
				// Android 3.x
				n = builder.getNotification();
			}
		} else {
			// Android 2.2
			n = new Notification(icon, tickerText, System.currentTimeMillis());
			
			// Informaçoes
			n.setLatestEventInfo(context, title, message, p);
		}
		// Tempo de vibração em forma de lista 
		n.vibrate = new long[]{ 0, 100, 400, 500 };
		
		// id (numero único) que identifica esta notificao
		NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		nm.notify(id, n);
	}

	/**
	 * API Level 17 - Android 4.2 ou superior
	 */
	@SuppressLint("NewApi")
	public static void create_v17(Context context, String tickerText,
			String title, String message, int icon, int id, Intent intent) {
		// PendingIntent para executar a intent ao selecionar a notificacao
		PendingIntent p = PendingIntent.getActivity(context, 0, intent, 0);

		Notification.Builder builder = new Notification.Builder(context);
		builder.setContentTitle(tickerText);
		builder.setContentText(message);
		builder.setSmallIcon(icon);
		/*
		 * builder.setLargeIcon(BitmapFactory.decodeResource(
		 * context.getResources(), R.drawable.ic_launcher));
		 */
		builder.setContentIntent(p);

		// Android 4.2
		Notification n = builder.build();

		// id (numero único) que identifica esta notificacao
		NotificationManager nm = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		nm.notify(id, n);
	}

	/**
	 * API Level 17 - Android 4.2 ou superior
	 */
	@SuppressLint("NewApi")
	public static void create_v17_big(Context context, String tickerText,
			String title, String message, String[] lines, int icon, int id,
			Intent intent) {

		// PendingIntent para executar a intent ao selecionar a notificacao
		PendingIntent p = PendingIntent.getActivity(context, 0, intent, 0);

		Notification.Builder builder = new Notification.Builder(context);
		builder.setContentTitle(tickerText);
		builder.setContentText(message);
		builder.setSmallIcon(icon);
		/*
		 * builder.setLargeIcon(BitmapFactory.decodeResource(
		 * context.getResources(), R.drawable.ic_launcher));
		 */
		builder.setContentIntent(p);

		// Cria o estilo detalhado
		Notification.InboxStyle style = new Notification.InboxStyle();
		style.setBigContentTitle(tickerText);

		for (String line : lines) {
			style.addLine(line);
		}

		// Informa o estilo
		builder.setStyle(style);
		// builder.setProgress(0, 0, true);

		// Android 4.2
		Notification n = builder.build();

		// id (numero único) que identifica esta notificacao
		NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		nm.notify(id, n);
	}

	@SuppressLint("NewApi")
	public static void generateNotification(Context context, String message,Intent notificationIntent) {

		NotificationManager notificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);

		PendingIntent intent = PendingIntent.getActivity(context, 0,
				notificationIntent, 0);

		String title = context.getString(R.string.app_name);
		Notification.Builder builder = new Notification.Builder(context)
				.setContentTitle(message).setContentText(title)
				.setContentIntent(intent).setSmallIcon(R.drawable.ic_launcher);
		
		Notification notification = builder.build();

		// Configura a intent para abrir a activity no topo, somente se nao
		// estiver aberta
		// notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
		// Intent.FLAG_ACTIVITY_SINGLE_TOP);

		notification.flags |= Notification.FLAG_AUTO_CANCEL;
		notificationManager.notify(0, notification);
	}

	public static void cancell(Context context, int id) {
		NotificationManager nm = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		nm.cancel(id);
	}

}
