package br.taxyii.library.utils;

import java.util.Calendar;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo.State;
import android.widget.Toast;
import br.taxyii.library.task.DefaultTask;


public class AndroidUtils {

	/* Toasts */

	public static void showSimpleToast(Activity activity, int message,int duracao) {
		Toast.makeText(activity, activity.getString(message), duracao).show();
	}

	/* Progress Dialog */

	public static ProgressDialog showProgressDialog(Activity activity,String title, String menssage, int icon) {
		ProgressDialog progress = new ProgressDialog(activity);
		progress.setTitle(title);
		progress.setMessage(menssage);

		if (icon > 0)
			progress.setIcon(icon);

		return progress;
	}

	/* Alert Dialog com Intencao */

	public static void showAlertDialogComIntencao(final Activity activity,String title, String message, final Intent i) {

		AlertDialog.Builder dialog = new AlertDialog.Builder(activity);

		dialog.setTitle(title).setMessage(message);

		dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (i != null) {
					activity.startActivity(i);
					activity.finish();
				} else {
					dialog.dismiss();
				}
			}
		});
		dialog.show();
	}

	/* Alert Dialog de confirmação */

	public static void showAlertDialogConfirm(final Activity activity,String title, String message, final DefaultTask task) {
		AlertDialog.Builder dialog = new AlertDialog.Builder(activity);

		dialog.setTitle(title).setMessage(message);

		dialog.setPositiveButton("OK", new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				task.execute();
				dialog.dismiss();
			}
		});

		dialog.setNegativeButton("Cancelar", new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		dialog.show();
	}
	
	/* Verificar Conexao com internet */

	public static boolean checkConnection(Activity activity) {

		boolean conectado = false;

		ConnectivityManager conn = (ConnectivityManager) activity.getBaseContext().getSystemService(Context.CONNECTIVITY_SERVICE);
		// verifica se existe conexão 3G ou WiFi
		if (conn.getNetworkInfo(0).getState() == State.CONNECTED
				|| conn.getNetworkInfo(1).getState() == State.CONNECTED) {
			conectado = true;
		}

		return conectado;
	}
	
	
	public static boolean checkConnection(Context context) {

		boolean conectado = false;

		ConnectivityManager conn = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
		// verifica se existe conexão 3G ou WiFi
		if (conn.getNetworkInfo(0).getState() == State.CONNECTED
				|| conn.getNetworkInfo(1).getState() == State.CONNECTED) {
			conectado = true;
		}

		return conectado;
	}
	/* Configurar um Alarm */

	public static void setAlarm(Activity activity, int segundos, Intent intent) {
		PendingIntent p = PendingIntent.getService(activity, 0, intent, 0);

		Calendar c = Calendar.getInstance();

		c.setTimeInMillis(System.currentTimeMillis());

		c.add(Calendar.SECOND, segundos);

		long time = c.getTimeInMillis();

		AlarmManager alarm = (AlarmManager) activity.getSystemService(Context.ALARM_SERVICE);

		alarm.set(AlarmManager.RTC_WAKEUP, time, p);
	}

	/*
	 * Metodo responsavel por verificar se o gps esta habilitado, caso não 
	 * esteja, mostrará um alerta pedindo para o usuario habilitar
	 */
	
	public static boolean checkGPSEnable(Activity activity){
		
		LocationManager service = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
		
		boolean enabled = service.isProviderEnabled(LocationManager.GPS_PROVIDER);
		
		return enabled;
	}
}
