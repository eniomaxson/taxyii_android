package br.taxyii.library.utils;

import java.io.IOException;
import java.io.InputStream;
import java.net.SocketException;

import org.apache.commons.net.ftp.FTPClient;

public class FtpClientUtils {

	public static final String dirRemotoDocumento = "documentos";
	public static final String dirRemotoFoto = "fotos";
	private FTPClient ftpClient;
	private InputStream file;
	private String host;
	private String login;
	private String password;

	public FtpClientUtils(FTPClient ftpClient, InputStream file, String host,String login, String password) {
		this.ftpClient = ftpClient;
		this.file = file;
		this.host = host;
		this.login = login;
		this.password = password;
	}

	public boolean upload(String dirRemoto) {

		boolean enviado = false;

		ftpClient = new FTPClient();
		try {
			ftpClient.connect(host);

			if (ftpClient.isConnected()) {
				if (ftpClient.login(login, password)) {
					enviado = ftpClient.storeFile(dirRemoto, file);
				}
			}
		} catch (SocketException e) {
			return enviado;
		} catch (IOException e) {
			return enviado;
		}
		return enviado;
	}

}
