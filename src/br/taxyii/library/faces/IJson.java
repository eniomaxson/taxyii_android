package br.taxyii.library.faces;

import org.json.JSONArray;
import org.json.JSONObject;

public interface IJson {

	public abstract Object toObject(JSONObject jsonObject);

	public abstract String toJson();

	public abstract Object toCollectionObject(JSONArray jsonArray);
}
