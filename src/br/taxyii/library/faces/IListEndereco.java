package br.taxyii.library.faces;

import android.location.Location;

public interface IListEndereco {
	
	public abstract void findAddress(Location location);
	
	public abstract void formatListEndereco();
	
}
