package br.taxyii.library.faces;

public interface ITransacao {

	public void executar() throws Exception;
	
	public void atualizarView();
	
}
