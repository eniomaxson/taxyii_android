package br.taxyii.library.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import br.taxyii.library.model.ModeloModel;


public class ListModeloAdapter extends BaseAdapter {
	private Context context;
	private ArrayList<ModeloModel> modelos;

	public ListModeloAdapter(Context ctx, ArrayList<ModeloModel> modelos) {
		this.context = ctx;
		this.modelos = modelos;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return modelos.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return modelos.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return modelos.get(position).getId();
	}

	public int getIndexByID(int id) {
		for (int i = 0; i < modelos.size(); i++) {
			ModeloModel m = (ModeloModel) getItem(i);
			if (m.getId() == id) {
				return i;
			}
		}
		return -1;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		TextView textview = (TextView) inflater.inflate(android.R.layout.simple_spinner_item, null);
		textview.setText(modelos.get(position).getDescricao());
		return textview;
	}

}
