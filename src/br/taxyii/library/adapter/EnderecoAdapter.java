package br.taxyii.library.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import br.taxyii.library.R;
import br.taxyii.library.model.EnderecoModel;


public class EnderecoAdapter extends BaseAdapter {

	private ArrayList<EnderecoModel> enderecos;
	private Context context;

	public EnderecoAdapter(Context context, ArrayList<EnderecoModel> enderecos) {
		this.enderecos = enderecos;
		this.context = context;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return enderecos.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return enderecos.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return enderecos.get(position).getId();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		EnderecoModel end = (EnderecoModel) getItem(position);

		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.template_list_endereco_proximo, null); 
		
		TextView nome = (TextView) view.findViewById(R.id.endereco_nome);	
		TextView rua = (TextView) view.findViewById(R.id.rua);
		TextView numero = (TextView) view.findViewById(R.id.numero);
		TextView bairro = (TextView) view.findViewById(R.id.bairro);
		
		nome.setText(end.getNome());
		
		if(end.getNumero() != null)
			numero.setText(end.getNumero());
		else
			numero.setVisibility(View.GONE);
		
		rua.setText(end.getRua());
			
		bairro.setText(end.getBairro() +" - "+ end.getCidade());
		
		return view;
	}

}
