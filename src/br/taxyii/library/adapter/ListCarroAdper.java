package br.taxyii.library.adapter;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import br.taxyii.library.R;
import br.taxyii.library.model.TaxiModel;


public class ListCarroAdper extends BaseAdapter {

	private ArrayList<TaxiModel> taxis;
	private Context context;

	public ListCarroAdper(Context c, ArrayList<TaxiModel> t) {
		context = c;
		taxis = t;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return taxis.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return taxis.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return taxis.get(position).getId();
	}

	@SuppressLint("ResourceAsColor")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		TaxiModel taxi = taxis.get(position);

		LayoutInflater layoutInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View v = layoutInflater.inflate(R.layout.template_list_carro, null);

		TextView modelo = (TextView) v.findViewById(R.id.lblModelo);

		TextView marca = (TextView) v.findViewById(R.id.lblMarca);

		ImageView sts = (ImageView) v.findViewById(R.id.imgStatus);

		modelo.setText(taxi.getModelo().getDescricao());
		marca.setText(taxi.getModelo().getMarca().getDescricao());

		if (taxi.getPrincipal() > 0) {
			sts.setImageResource(R.drawable.ic_action_important);
		}

		return v;
	}

}
