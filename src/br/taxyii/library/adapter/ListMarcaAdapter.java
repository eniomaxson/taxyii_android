package br.taxyii.library.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import br.taxyii.library.model.MarcaModel;


public class ListMarcaAdapter extends BaseAdapter {
	private ArrayList<MarcaModel> marcas;
	private Context context;

	public ListMarcaAdapter(Context ctx, ArrayList<MarcaModel> marcas) {
		this.context = ctx;
		this.marcas = marcas;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return marcas.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return marcas.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return marcas.get(position).getId();
	}

	protected long getPositionById() {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		MarcaModel marca = (MarcaModel) getItem(position);

		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		TextView textview = (TextView) inflater.inflate(
				android.R.layout.simple_spinner_item, null);

		textview.setText(marca.getDescricao());

		return textview;
	}

	public int getIndexByID(int id) {
		for (int i = 0; i < marcas.size(); i++) {
			MarcaModel m = (MarcaModel) getItem(i);
			if (m.getId() == id) {
				return i;
			}
		}
		return -1;
	}

}
