package br.taxyii.library.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;
import br.taxyii.library.model.FormaPagamentoModel;


public class FormaPagamentoAdapter extends BaseAdapter {
	private ArrayList<FormaPagamentoModel> formasPagamentos;
	private Context contexto;

	public FormaPagamentoAdapter(Context ctx,
			ArrayList<FormaPagamentoModel> frmPgts) {
		this.contexto = ctx;
		this.formasPagamentos = frmPgts;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return formasPagamentos.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return formasPagamentos.get(position);
	}

	@Override
	public long getItemId(int position) {

		return formasPagamentos.get(position).getId();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		FormaPagamentoModel frmPg = (FormaPagamentoModel) getItem(position);
		LayoutInflater inflate = (LayoutInflater) contexto.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View v = inflate.inflate(android.R.layout.simple_list_item_multiple_choice, null);

		CheckedTextView descricao = (CheckedTextView) v.findViewById(android.R.id.text1);
		
		descricao.setText(frmPg.getDescricao());
		
		descricao.setChecked(frmPg.getAceita() > 0 ? true: false);

		return v;
	}
}
