package br.taxyii.library.adapter;

import java.util.ArrayList;

import br.taxyii.library.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ConfiguracaoAdapter extends BaseAdapter{
	
	private ArrayList<ArrayList<Object>> items;
	private Context context;
	
	public ConfiguracaoAdapter(Context context, ArrayList<ArrayList<Object>> items) {
		this.context = context;
		this.items = items;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return items.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return items.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		@SuppressWarnings("unchecked")
		ArrayList<Object> item = (ArrayList<Object>) getItem(position);
		
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View v = inflater.inflate(R.layout.template_list_configuracao, null);
		ImageView icon = (ImageView) v.findViewById(R.id.icon);
		TextView descricao = (TextView) v.findViewById(R.id.descricao);
		
		icon.setBackgroundResource((Integer) item.get(0));
		descricao.setText((String) item.get(1));
		
		return v;
	}

}
