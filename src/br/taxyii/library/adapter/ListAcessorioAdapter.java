package br.taxyii.library.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import br.taxyii.library.model.AcessorioModel;


public class ListAcessorioAdapter extends BaseAdapter {
	private ArrayList<AcessorioModel> acessorios;
	private Context context;

	public ListAcessorioAdapter(Context ctx,
			ArrayList<AcessorioModel> acessorios) {
		this.context = ctx;
		this.acessorios = acessorios;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return acessorios.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return acessorios.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return acessorios.get(position).getId();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		AcessorioModel acessorio = (AcessorioModel) getItem(position);

		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		TextView textview = (TextView) inflater.inflate(android.R.layout.simple_spinner_item, null);

		textview.setText(acessorio.getDescricao());

		return textview;
	}

}
