package br.taxyii.library.transacao;

import android.content.Context;
import br.taxyii.library.utils.GcmUtils;


public class TRegistrarGCM implements Runnable {

	public String SENDER_ID;
	public Context context;

	public TRegistrarGCM(String id, Context context) {
		this.SENDER_ID = id;
		this.context = context;
	}

	@Override
	public void run() {
		GcmUtils utils = new GcmUtils(context);
		utils.register(SENDER_ID);
	}

}
