package br.taxyii.library.fragment;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import br.taxyii.library.R;

import com.actionbarsherlock.app.SherlockFragment;

public class ProgressFragment extends SherlockFragment {
	private View progress_view;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.fragment_progress, null);

		progress_view = view;

		return view;
	}

	@SuppressLint("NewApi")
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	public void showProgress(final boolean show, final View principal) {
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			int shortAnimTime = getResources().getInteger(
					android.R.integer.config_shortAnimTime);

			progress_view.setVisibility(View.VISIBLE);
			progress_view.animate().setDuration(shortAnimTime)
					.alpha(show ? 1 : 0)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							progress_view.setVisibility(show ? View.VISIBLE
									: View.GONE);
						}
					});

			principal.setVisibility(View.VISIBLE);
			principal.animate().setDuration(shortAnimTime).alpha(show ? 0 : 1)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							principal.setVisibility(show ? View.GONE
									: View.VISIBLE);
						}
					});
		} else {
			progress_view.setVisibility(show ? View.VISIBLE : View.GONE);
			principal.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}

}
