package br.taxyii.library.fragment;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.Toast;
import br.taxyii.library.R;
import br.taxyii.library.adapter.EnderecoAdapter;
import br.taxyii.library.faces.ITransacao;
import br.taxyii.library.model.EnderecoModel;
import br.taxyii.library.task.DefaultTask;
import br.taxyii.library.utils.AndroidUtils;


public class ListaEnderecoFragment extends TaxyiiFragment implements OnItemClickListener, OnItemLongClickListener{
	private ArrayList<EnderecoModel> enderecos;
	private String response;
	private EnderecoAdapter enderecoAdapter;
	private ListView list;
	private String confirmarSolicitacao; 
	private String KEY_STATE = "endereco_favorito";
	
	@SuppressWarnings("unchecked")
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
		if(savedInstanceState != null){
			
			if(savedInstanceState.containsKey(KEY_STATE)){
				String  jsonArray = savedInstanceState.getString(KEY_STATE);
				
				try {
					enderecos = (ArrayList<EnderecoModel>) new EnderecoModel().toCollectionObject(new JSONArray(jsonArray));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		
		if(enderecos != null){
			enderecoAdapter = new EnderecoAdapter(getActivity().getBaseContext(), enderecos);
			list.setAdapter(enderecoAdapter);
			
		}else{
			startTransacao();
		}
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		
		setRetainInstance(true);
		
		View view = inflater.inflate(R.layout.fragment_list_endereco_favorito,null);
		
		formView = view.findViewById(R.id.formView);
		
		statusView = view.findViewById(R.id.pgBar);
		
		list = (ListView) view.findViewById(R.id.listEndereco);
		
		list.setOnItemClickListener(this);

		list.setOnItemLongClickListener(this);

		return view;
	}
	
	@Override
	public void executar() throws Exception {
		response = new EnderecoModel().listEndereco(getActivity().getBaseContext());
	}

	@SuppressWarnings("unchecked")
	@Override
	public void atualizarView() {
		if (response != null) {
			if (!response.contains("error")) {
				try {
					enderecos = (ArrayList<EnderecoModel>) new EnderecoModel().toCollectionObject(new JSONArray(response));

					enderecoAdapter = new EnderecoAdapter(getActivity().getBaseContext(), enderecos);

					list.setAdapter(enderecoAdapter);

				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		//progress.showProgress(false, viewList);
	}
	
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,long id) {
		
		EnderecoModel selecionado = (EnderecoModel) enderecoAdapter.getItem(position);
		if(confirmarSolicitacao==null)
			confirmarSolicitacao = "SOLICITAR_TAXI";
		
		Intent i = new Intent(confirmarSolicitacao);
		
		i.putExtra("endereco", selecionado.toJson());
		
		startActivity(i);
	}

	private class TExcluir implements ITransacao {
		private int position;

		@Override
		public void executar() throws Exception {

			EnderecoModel end = (EnderecoModel) enderecoAdapter.getItem(position);

			response = end.deletar();
		}

		@Override
		public void atualizarView() {
			if (response != null) {
				enderecos.remove(position);
				list.setAdapter(enderecoAdapter);
				AndroidUtils.showSimpleToast(getActivity(),R.string.sucessoRemoverEndereco, Toast.LENGTH_SHORT);
			}
		}
	}

	@SuppressLint("NewApi")
	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		
		if(outState != null){
			if(outState.containsKey(KEY_STATE)){
				outState.remove(KEY_STATE);
			}
		}
		if(enderecos != null)
			outState.putString(KEY_STATE, new EnderecoModel().toJsonArray(enderecos).toString());
	}
	
	@Override
	public boolean onItemLongClick(AdapterView<?> parent, View view,int position, long id) {
		TExcluir t = new TExcluir();

		t.position = position;

		DefaultTask task = new DefaultTask(getActivity(), t,getString(R.string.pgTituloEndereco),getString(R.string.pgMensagemEndereco), 0);

		if (AndroidUtils.checkConnection(getActivity())) {

			AndroidUtils.showAlertDialogConfirm(getActivity(),getString(R.string.alertTituloDelete),getString(R.string.alertMensagemDelete), task);

		} else {
			AndroidUtils.showSimpleToast(getActivity(), R.string.error_conexao,Toast.LENGTH_SHORT);
		}

		return false;
	}

	public String getAcaoConfirmar() {
		return confirmarSolicitacao;
	}

	public void setAcaoConfirmar(String acao) {
		this.confirmarSolicitacao = acao;
	}

}
