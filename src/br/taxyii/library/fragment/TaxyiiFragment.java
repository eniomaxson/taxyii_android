package br.taxyii.library.fragment;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.os.Build;
import android.view.View;
import android.widget.Toast;
import br.taxyii.library.R;
import br.taxyii.library.faces.ITransacao;
import br.taxyii.library.task.FragmentTask;
import br.taxyii.library.utils.AndroidUtils;

import com.actionbarsherlock.app.SherlockFragment;

@SuppressLint("NewApi")
public class TaxyiiFragment extends SherlockFragment implements ITransacao {

	boolean redeOk;
	protected int progressId;
	protected FragmentTask task;
	public View statusView;
	public View formView;
	
	public void startTransacao() {
		redeOk = AndroidUtils.checkConnection(getActivity());

		if (redeOk) {
			task = new FragmentTask(this, this);
			task.execute();
		} else {
			AndroidUtils.showSimpleToast(getActivity(), R.string.error_conexao,Toast.LENGTH_LONG);
		}
	}

	@Override
	public void executar() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void atualizarView() {
		// TODO Auto-generated method stub

	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	public void showProgress(final boolean show) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			int shortAnimTime = getResources().getInteger(
					android.R.integer.config_shortAnimTime);

			statusView.setVisibility(View.VISIBLE);
			statusView.animate().setDuration(shortAnimTime)
					.alpha(show ? 1 : 0)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							statusView.setVisibility(show ? View.VISIBLE
									: View.GONE);
						}
					});

			formView.setVisibility(View.VISIBLE);
			formView.animate().setDuration(shortAnimTime)
					.alpha(show ? 0 : 1)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							formView.setVisibility(show ? View.GONE
									: View.VISIBLE);
						}
					});
		} else {
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant UI components.
			statusView.setVisibility(show ? View.VISIBLE : View.GONE);
			formView.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}

	@Override
	public void onStop() {
		super.onStop();
		if (task != null){
			task.cancel(true);
			task = null;
		}
			
	}
}
