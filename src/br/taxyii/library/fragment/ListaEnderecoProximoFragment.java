package br.taxyii.library.fragment;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import br.taxyii.library.R;
import br.taxyii.library.adapter.EnderecoAdapter;
import br.taxyii.library.model.EnderecoModel;
import br.taxyii.library.model.LocalizacaoModel;
import br.taxyii.library.utils.AndroidUtils;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

public class ListaEnderecoProximoFragment extends TaxyiiFragment implements OnItemClickListener {

	private ListView enderecoProximos;
	private EnderecoAdapter enderecoAdapter;
	private ArrayList<EnderecoModel> enderecos;
	protected ProgressFragment progress;
	private int LOCALIZE = 1;
	private LocalizacaoModel localizacao;
	private List<Address> address;
	private Location location;
	private String acaoConfirmar;
	private String KEY_STATE = "endereco_proximo";
	
	@SuppressWarnings("unchecked")
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
		setHasOptionsMenu(true);
		
		if (!AndroidUtils.checkGPSEnable(getActivity())) {
			Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
			AndroidUtils.showAlertDialogComIntencao(getActivity(),getString(R.string.gps_titulo),getString(R.string.gps_message), intent);
		} 
		
		if(savedInstanceState != null){
			
			if(savedInstanceState.containsKey(KEY_STATE)){
				String  jsonArray = savedInstanceState.getString(KEY_STATE);
				
				try {
					enderecos = (ArrayList<EnderecoModel>) new EnderecoModel().toCollectionObject(new JSONArray(jsonArray));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
			
		if(enderecos != null){
			if(enderecos.size() > 0){
				enderecoAdapter = new EnderecoAdapter(getActivity().getBaseContext(),enderecos);
				enderecoProximos.setAdapter(enderecoAdapter);
				enderecoProximos.setOnItemClickListener(this);
				showProgress(false);
			}
		}
		
		localizacao = new  LocalizacaoModel(getActivity().getBaseContext(),this,5);
		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View v = inflater.inflate(R.layout.fragment_list_endereco_proximo, null);

		enderecoProximos = (ListView) v.findViewById(R.id.listEnderecoProximo);

		formView = v.findViewById(R.id.formView);

		statusView = v.findViewById(R.id.pgBar);

		return v;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		menu.add(0,LOCALIZE,0,"Localizi-me").setIcon(R.drawable.ic_action_location_searching).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		int id = item.getItemId();
		
		if(id == LOCALIZE)
		{
			if(task != null)
				task.cancel(true);
			localizacao.atualizarView();
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void atualizarView() {
		enderecos = new ArrayList<EnderecoModel>();
		String cidade = null;
		String bairro = null;

		if (address != null) {
			if(address.size() > 0){
				for (Address adr : address) {
	
					EnderecoModel endereco = new EnderecoModel();
	
					bairro = adr.getSubLocality();
	
					cidade = adr.getSubAdminArea() + " / RN";
					endereco.setRua(adr.getThoroughfare());
					endereco.setBairro(bairro);
					endereco.setCidade(cidade);
					endereco.setCoordenada(adr.getLatitude() + "," + adr.getLongitude());
					endereco.setNome(getString(R.string.lblEnderecoAtual));
					enderecos.add(endereco);
				}
	
				enderecoAdapter = new EnderecoAdapter(getActivity().getBaseContext(), enderecos);
				enderecoProximos.setAdapter(enderecoAdapter);
				enderecoProximos.setOnItemClickListener(this);
			}	
		}
		showProgress(false);
	}

	@Override
	public void executar() throws Exception {
		
		location = localizacao.getCurrent_location();
		
		if (location != null) {
			Geocoder geocoder = new Geocoder(getActivity().getBaseContext(), Locale.ENGLISH);	
			while(address == null)
			{		
				try {	
					address = geocoder.getFromLocation(location.getLatitude(),location.getLongitude(), 1);
				} catch (IOException e1) {
					address = null;
				} catch (IllegalArgumentException e2) {
					address = null;
				}
			}
		}
	}

	@SuppressLint("NewApi")
	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		
		if(outState != null){
			if(outState.containsKey(KEY_STATE)){
				outState.remove(KEY_STATE);
			}
		}
		if(enderecos != null)
			outState.putString(KEY_STATE, new EnderecoModel().toJsonArray(enderecos).toString());
	}

	
	public String getAcaoConfirmar() {
		return acaoConfirmar;
	}

	
	public void setAcaoConfirmar(String acaoConfirmar) {
		this.acaoConfirmar = acaoConfirmar;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,long id) {
		EnderecoModel selecionado = (EnderecoModel) enderecoAdapter.getItem(position);
		
		if(acaoConfirmar==null)
			acaoConfirmar = "SOLICITAR_TAXI";
		
		Intent i = new Intent(acaoConfirmar);
		
		i.putExtra("endereco", selecionado.toJson());
		
		startActivity(i);
	}
	
	
}

