package br.taxyii.library.fragment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import br.taxyii.library.R;
import br.taxyii.library.dao.UsuarioDAO;
import br.taxyii.library.model.CidadeModel;
import br.taxyii.library.model.TaxistaModel;
import br.taxyii.library.task.BuscarCidadeTask;
import br.taxyii.library.task.DefaultTask;
import br.taxyii.library.utils.AndroidUtils;
import br.taxyii.library.utils.IOUtils;
import br.taxyii.library.utils.ImageUtils;
import br.taxyii.library.utils.SDCardUtils;
import br.taxyii.library.utils.SharedPreferencesUtils;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

public class CadastroTaxistaFragment extends TaxyiiFragment{
	private EditText cep, cpf, numero_licenca, cidade;
	private Button btnFotoFrente, btnFotoVerso;
	private TaxistaModel taxista;
	private String dirName = "taxyii";
	private String docFrente = "doc_frente.jpg";
	private String docVerso = "doc_verso.jpg";
	private File file;
	private BuscarCidadeTask cidadeTask;
	private DefaultTask task;
	private int SALVAR = 1;
	private boolean validado = true;
	private String response = null;
	private boolean foto_verso_ok = false;
	private boolean foto_frente_ok = false;
	private ProgressFragment progress;
	private View frmCadastro;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_cadastro_taxista, null);
		
		cep = (EditText) view.findViewById(R.id.cep);
		
		cidade = (EditText) view.findViewById(R.id.cidade);
		
		cpf = (EditText) view.findViewById(R.id.cpf);
		
		numero_licenca = (EditText) view.findViewById(R.id.numero_licenca);
		
		btnFotoFrente = (Button) view.findViewById(R.id.btnFrente);
		
		btnFotoVerso = (Button) view.findViewById(R.id.btnVerso);
		
		cep.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus) {
					if (AndroidUtils.checkConnection(getActivity())) {
						if (!cep.getText().toString().equals("")) {
							cidadeTask = new BuscarCidadeTask();
							cidadeTask.activity = getActivity();
							cidadeTask.execute(cep.getText().toString());
							cidadeTask.cidade = cidade;
						}
					} else {
						AndroidUtils.showSimpleToast(getActivity(),R.string.error_conexao, Toast.LENGTH_SHORT);
					}
				} else {
					cep.setText("");
				}
			}
		});

		btnFotoFrente.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				file = SDCardUtils.getSdCardFile(dirName, docFrente);

				Intent i = new Intent("android.media.action.IMAGE_CAPTURE");

				i.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));

				startActivityForResult(i, 0);
			}
		});

		btnFotoVerso.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				file = SDCardUtils.getSdCardFile(dirName, docVerso);

				Intent i = new Intent("android.media.action.IMAGE_CAPTURE");

				i.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));

				startActivityForResult(i, 1);
			}
		});
		
		progress = (ProgressFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.pgCadastroTaxista);
		
		frmCadastro = view.findViewById(R.id.frmCadastroTaxista);
		
		setRetainInstance(true);
		
		return view;
	}
	
	public void setInputErros() {
		
		if (cpf.getText().toString().length() != 11) {
			cpf.setError(getString(R.string.error_cpf));
			validado = false;
		}

		if (numero_licenca.getText().toString().equals("")) {
			numero_licenca.setError(getString(R.string.error_licenca));
			validado = false;
		}

		if (cidade.getText().equals("")) {
			cep.setError(getString(R.string.error_campo_obrigatorio));
			validado = false;
		}
		
/*		if(!foto_frente_ok){
			AndroidUtils.showSimpleToast(getActivity(),R.string.msgDocFrente, Toast.LENGTH_SHORT);
			validado = false;
		}
		
		if(!foto_verso_ok){
			AndroidUtils.showSimpleToast(getActivity(),R.string.msgDocVerso, Toast.LENGTH_SHORT);			
			validado = false;
		}*/
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu,MenuInflater inflater) {
		menu.add(0, SALVAR, 0, "Salvar").setIcon(R.drawable.abs__ic_cab_done_holo_dark).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();

		if (id == SALVAR) {
			createTaxista();
		}
		return super.onOptionsItemSelected(item);
	}

	protected void createTaxista() {
		setInputErros();
		
		if (validado) {
			
			taxista = new TaxistaModel();
			
			taxista.setUsuario(new UsuarioDAO(getActivity().getBaseContext()).get());
			
			taxista.setCep(cep.getText().toString());
			
			taxista.setCpf(cpf.getText().toString());
			
			taxista.setLicenca(numero_licenca.getText().toString());
			
			taxista.setCep(cep.getText().toString());
			
			taxista.setCidade(new CidadeModel(cidade.getText().toString()));
			
			progress.showProgress(true, frmCadastro);
			
			startTransacao();
		}
		validado = true;
		
	}
	
	@Override
	public void executar() throws Exception {
		response = taxista.cadastrar();
	}
	
	@Override
	public void atualizarView() {
		progress.showProgress(false, frmCadastro);
		
		if (response != null) {
			if (!response.contains("error")) {
				SharedPreferencesUtils util = new SharedPreferencesUtils(getActivity(), SharedPreferencesUtils.STATUS_PREFERENCES);
				util.setPreferences(SharedPreferencesUtils.STATUS_TAXISTA, "A");
				util.setPreferences(SharedPreferencesUtils.STATUS_TAXI, "D");
				AndroidUtils.showAlertDialogComIntencao(getActivity(),"Registro enviado!","Transação realizada com sucesso!", new Intent("TAXISTA_HOME"));
			}
		}else{
			AndroidUtils.showSimpleToast(getActivity(), R.string.error_conexao, Toast.LENGTH_SHORT);
		}
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		super.onActivityResult(requestCode, resultCode, data);
		
		if (data != null) {
			Bundle bundle = data.getExtras();
			if (bundle != null) {
				Bitmap bitmap = ImageUtils.getResizedImage(Uri.fromFile(file),400, 600);
				
				try {
					FileInputStream in = new FileInputStream(file);
					
					if (requestCode == 1) {
						taxista.setDocumento_verso(IOUtils.toBytes(in));
					} else {
						taxista.setDocumento_frente(IOUtils.toBytes(in));
					}
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
