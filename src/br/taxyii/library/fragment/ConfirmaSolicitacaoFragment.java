package br.taxyii.library.fragment;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import br.taxyii.library.R;
import br.taxyii.library.dao.UsuarioDAO;
import br.taxyii.library.faces.ITransacao;
import br.taxyii.library.model.CorridaModel;
import br.taxyii.library.model.EnderecoModel;
import br.taxyii.library.model.UsuarioModel;
import br.taxyii.library.task.FragmentTask;
import br.taxyii.library.utils.AndroidUtils;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

public class ConfirmaSolicitacaoFragment extends TaxyiiFragment {

	private TextView bairro;
	private TextView rua;
	private TextView cidade;
	private EditText numero;
	private EditText descricao_endereco;
	
	private EditText ponto_referencia;
	private UsuarioModel passageiro;
	private EnderecoModel endereco;
	private CorridaModel corrida;
	// Menu
	private int SOLICITAR = 1;
	private int FAVORITAR = 2;
	
	private String response;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.fragment_confirma_solicitacao, null);

		formView = view.findViewById(R.id.frmConfirmarSolicitacao);
		
		statusView = view.findViewById(R.id.pgBar);
		
		cidade = (TextView) view.findViewById(R.id.cidade);
		
		bairro = (TextView) view.findViewById(R.id.bairro);

		rua = (TextView) view.findViewById(R.id.rua);

		numero = (EditText) view.findViewById(R.id.numero);

		ponto_referencia = (EditText) view.findViewById(R.id.ponto_referencia);

		setRetainInstance(true);

		return view;
	}

	@Override
	public void onResume() {
		
		String params = getActivity().getIntent().getStringExtra("endereco");
		
		try {
			endereco = (EnderecoModel) new EnderecoModel().toObject(new JSONObject(params));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		cidade.setText(endereco.getCidade());
		bairro.setText(endereco.getBairro());
		
		rua.setText(endereco.getRua());
		
		if(!endereco.getNumero().equals(""))
			numero.setText(endereco.getNumero());
		
		if(!endereco.getPonto_referencia().equals("null"))
			ponto_referencia.setText(endereco.getPonto_referencia());
			
		super.onResume();
	}
	
	@Override
	public void executar() throws Exception {
		response = corrida.solicitarTaxista();
	}
	
	@Override
	public void atualizarView() {
		if (response != null) {
			if (!response.contains("error")) {
				AndroidUtils.showAlertDialogComIntencao(getActivity(), getString(R.string.lblTituloConfirmacaoCorridaDialog), getString(R.string.lblMensagemConfirmacaoSucesso), new Intent("PASSAGEIRO_HOME"));
			}else{
				try {
					JSONObject j = new JSONObject(response).getJSONObject("error");
					
					if(j.has("passageiro_id")){
						AndroidUtils.showAlertDialogComIntencao(getActivity(), getString(R.string.lblTituloConfirmacaoCorridaDialog), getString(R.string.msgCorridaJaSolicitada), new Intent("ENDERECO_FAVORITO"));
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		response = null;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		menu.add(0, FAVORITAR, 1, "Endereço Favorito!").setIcon(R.drawable.ic_action_important).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		menu.add(0, SOLICITAR, 1, "Confirmar").setIcon(R.drawable.abs__ic_cab_done_holo_light).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		int menu_id = item.getItemId();

		if (menu_id == SOLICITAR) {
			if (ponto_referencia.getText().toString().equals("")) {
				ponto_referencia.setError("Informe um ponto de referencia!");
			} else {
				corrida = new CorridaModel();
				corrida.setPassageiro(new UsuarioDAO(getActivity().getBaseContext()).get());
				endereco.setNumero(numero.getText().toString());
				endereco.setPonto_referencia(ponto_referencia.getText().toString());
				corrida.setEndereco(endereco);
				startTransacao();
			}
		}
		
		if(menu_id == FAVORITAR){
			showDialog();
		}
		return super.onOptionsItemSelected(item);
	}

	public EnderecoModel getEndereco() {
		return endereco;
	}

	public void setEndereco(EnderecoModel endereco) {
		this.endereco = endereco;
	}
	
	public UsuarioModel getPassageiro() {
		return passageiro;
	}

	public void setPassageiro(UsuarioModel passageiro) {
		this.passageiro = passageiro;
	}
	
	public void favoritarEndereco(){
		new FragmentTask(this, new TFavoritarEndereco()).execute();
	}

	public void showDialog(){
		
		if(numero.getText().toString().equals("")){
			numero.setError(getString(R.string.msgCampoObrigatorio));
		
		}else{
			AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
			
			descricao_endereco = new EditText(getActivity().getBaseContext());
			
			descricao_endereco.setSingleLine();
			
			dialog.setView(descricao_endereco);
			
			dialog.setTitle("Derscrição para Endereço:");
			
			dialog.setPositiveButton("Confirmar", new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					favoritarEndereco();
					dialog.dismiss();
				}
			});

			dialog.setNegativeButton("Cancelar", new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			});
			
			dialog.show();	
		}
	}

	public class TFavoritarEndereco implements ITransacao {

		@Override
		public void executar() throws Exception {
			if(descricao_endereco.getText().toString().equals("")){
				descricao_endereco.setError(getString(R.string.msgCampoObrigatorio));
			}else{
				UsuarioModel passageiro = new UsuarioDAO(getActivity().getBaseContext()).get();
				endereco.setNumero(numero.getText().toString());
				endereco.setNome(descricao_endereco.getText().toString());
				endereco.setPass(passageiro);
				
				response = endereco.cadastrar();	
			}
		}

		@Override
		public void atualizarView() {
			if(response != null){
				if(!response.contains("error")){
					AndroidUtils.showSimpleToast(getActivity(),R.string.msgEnderecoAdicionado, Toast.LENGTH_SHORT);						
				}else{
					AndroidUtils.showSimpleToast(getActivity(), R.string.msgEnderecoJaAdicionado, Toast.LENGTH_SHORT);
				}
			}
			response = null;
		}
		
	}
}
