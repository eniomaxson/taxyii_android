package br.taxyii.library.fragment;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;
import br.taxyii.library.R;
import br.taxyii.library.dao.UsuarioDAO;
import br.taxyii.library.model.UsuarioModel;
import br.taxyii.library.utils.AndroidUtils;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

public class LoginFragment extends TaxyiiFragment implements OnClickListener {

	protected String email;
	protected String senha;
	protected EditText mEmailView;
	protected EditText mPasswordView;
	protected Button btnLogin;
	private Intent intencao;
	protected String response = null;
	private String acaoNovoUser;
	private int NOVO_USUARIO = 1;
	private int RECUPERAR_SENHA = 2;
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		setHasOptionsMenu(true);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.fragment_login, null);
		
		formView = (LinearLayout) view.findViewById(R.id.frmView);
		
		statusView= (LinearLayout) view.findViewById(R.id.pgBar);
		
		mEmailView = (EditText) view.findViewById(R.id.email);

		mPasswordView = (EditText) view.findViewById(R.id.password);

		btnLogin = (Button) view.findViewById(R.id.btnLogin);

		btnLogin.setOnClickListener(this);

		return view;
	}

	@Override
	public void executar() throws Exception {

		JSONObject login = new JSONObject();

		try {
			login.put("email", email);
			login.put("senha", senha);

			response = new UsuarioModel().login(login);

		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void atualizarView() {
		if (response != null) {
			try {
				if (response.contains("error")) {
					JSONObject json = new JSONObject(response);
					json = new JSONObject(json.getString("error"));
					AndroidUtils.showSimpleToast(getActivity(),
							R.string.error_login, Toast.LENGTH_SHORT);
				} else {
					JSONObject json = new JSONObject(response);
					UsuarioModel usuario = (UsuarioModel) new UsuarioModel().toObject(json);
					UsuarioDAO dao = new UsuarioDAO(getActivity().getBaseContext());
					dao.inserir(usuario);
					dao.fechar();
					startActivity(intencao);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onClick(View v) {
		email = mEmailView.getText().toString();

		senha = mPasswordView.getText().toString();

		if (!email.equals("") && !senha.equals("")) {
			startTransacao();
		} else {
			mEmailView.setError(getString(R.string.error_campo_obrigatorio));
			mPasswordView.setError(getString(R.string.error_campo_obrigatorio));
		}
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		menu.add(0, NOVO_USUARIO, 0, "Primeiro Acesso");
		menu.add(0, RECUPERAR_SENHA, 0, "Recuperar Senha");
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		int opcao = item.getItemId();

		if (opcao == RECUPERAR_SENHA) {

		}

		if (opcao == NOVO_USUARIO) {
			startActivity(new Intent(getIntentPrimeiroAcesso_novo_usuario()));
		}
		return super.onOptionsItemSelected(item);
	}

	public Intent getIntencao() {
		return intencao;
	}

	public void setIntencao(Intent intencao) {
		this.intencao = intencao;
	}

	/**
	 * @return the acao_novo_usuario
	 */
	public String getIntentPrimeiroAcesso_novo_usuario() {
		return acaoNovoUser;
	}

	/**
	 * @param acao_novo_usuario
	 *            the acao_novo_usuario to set
	 */
	public void setIntentPrimeiroAcesso(String acao_novo_usuario) {
		this.acaoNovoUser = acao_novo_usuario;
	}

}
