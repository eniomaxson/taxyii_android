package br.taxyii.library.fragment;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import br.taxyii.library.R;
import br.taxyii.library.adapter.ListMarcaAdapter;
import br.taxyii.library.adapter.ListModeloAdapter;
import br.taxyii.library.dao.UsuarioDAO;
import br.taxyii.library.faces.ITransacao;
import br.taxyii.library.model.MarcaModel;
import br.taxyii.library.model.ModeloModel;
import br.taxyii.library.model.TaxiModel;
import br.taxyii.library.model.TaxistaModel;
import br.taxyii.library.model.UsuarioModel;
import br.taxyii.library.task.DefaultTask;
import br.taxyii.library.utils.AndroidUtils;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

public class CadastroTaxiFragment extends TaxyiiFragment {

	private Spinner marcas, modelos;
	private EditText ano, placa;
	private ArrayList<ModeloModel> listModelos;
	private ArrayList<MarcaModel> listMarcas;
	private ListMarcaAdapter marcaAdapter;
	private ListModeloAdapter modeloAdapter;
	private String response;
	private long marca_selecionada;
	private TaxiModel taxi;
	private int SALVAR = 1;
	private boolean validado = true;
	private CheckBox principal;
	private CheckBox comAr;
	private boolean valor_carregado = false;
	private ProgressFragment progress;
	private View frmTaxi;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		new DefaultTask(new Tmarca()).execute();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		
		View view = inflater.inflate(R.layout.fragment_cadastrar_taxi,null);
		
		progress = (ProgressFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.pgCadastroTaxi);
			
		frmTaxi = view.findViewById(R.id.frmCadastroTaxi);
		
		marcas = (Spinner) view.findViewById(R.id.marcas);
		
		modelos = (Spinner) view.findViewById(R.id.modelos);
		
		placa = (EditText) view.findViewById(R.id.placa);
		
		ano = (EditText) view.findViewById(R.id.ano);
		
		principal = (CheckBox) view.findViewById(R.id.rdPrincipal);
		
		comAr = (CheckBox) view.findViewById(R.id.comAr);
		
		marcas.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,int position, long arg3) {
				
				marca_selecionada = marcaAdapter.getItemId(position);

				new DefaultTask(new Tmodelo()).execute();
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
			}
		});
		
		setRetainInstance(true);
		
		return view;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		menu.add(0, SALVAR, 0, "Salvar").setIcon(R.drawable.abs__ic_cab_done_holo_dark).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		int id = item.getItemId();

		if (id == SALVAR) {
			setErros();

			createTaxi();

			if(validado)
				startTransacao();
			
		}
		
		validado = true;
		
		return super.onOptionsItemSelected(item);
	}

	private void createTaxi() {
		if (validado) {
			if (taxi == null) {
				
				taxi = new TaxiModel();
				
				UsuarioModel usuario = new UsuarioDAO(getActivity().getBaseContext()).get();
				
				taxi.setPlaca(placa.getText().toString());
				
				taxi.setAno(Integer.valueOf(ano.getText().toString()));
				
				taxi.setModelo((ModeloModel) modeloAdapter.getItem(modelos.getSelectedItemPosition()));
				
				taxi.setTaxista(new TaxistaModel(usuario));

				if (principal.isChecked()) {
					taxi.setPrincipal(1);
				} else {
					taxi.setPrincipal(0);
				}
				if (comAr.isChecked()) {
					taxi.setCom_ar(1);
				} else {
					taxi.setCom_ar(0);
				}

			} else {
				taxi.setPlaca(placa.getText().toString());
				
				taxi.setAno(Integer.valueOf(ano.getText().toString()));
				
				taxi.setModelo((ModeloModel) modeloAdapter.getItem(modelos.getSelectedItemPosition()));

				if (principal.isChecked()) {
					taxi.setPrincipal(1);
				} else {
					taxi.setPrincipal(0);
				}

				if (comAr.isChecked()) {
					taxi.setCom_ar(1);
				} else {
					taxi.setCom_ar(0);
				}
			}
		}
	}

	private void setErros() {
		if (placa.getText().toString().length() != 7) {
			placa.setError("Informe uma placa valida!");
			validado = false;
		}
		if (ano.getText().toString().length() != 4) {
			ano.setError("Informe uma placa valida!");
			validado = false;
		}
	}

	@Override
	public void executar() throws Exception {
		
		if (taxi.getId() > 0) {
			response = taxi.alterar();
		} else {
			response = taxi.cadastrar();
		}
	}
	
	@Override
	public void atualizarView() {
		if (response != null) {
			if (!response.contains("error")) {
				startActivity(new Intent("LISTAR_TAXI"));
			}
		}else{
			AndroidUtils.showSimpleToast(getActivity(), R.string.error_conexao, Toast.LENGTH_LONG);
		}
	}
	
	private class Tmarca implements ITransacao {

		@Override
		public void executar() throws Exception {
			// TODO Auto-generated method stub
			listMarcas = new MarcaModel().listMarca(getActivity().getBaseContext());
		}

		@Override
		public void atualizarView() {
			if (listMarcas != null) {
				marcaAdapter = new ListMarcaAdapter(getActivity().getBaseContext(),listMarcas);
				marcas.setAdapter(marcaAdapter);
			}
		}

	}

	private class Tmodelo implements ITransacao {

		@Override
		public void executar() throws Exception {

			listModelos = new ModeloModel().listModelo(getActivity().getBaseContext(),marca_selecionada);
		}

		@Override
		public void atualizarView() {
			if (listModelos != null) {
				
				modeloAdapter = new ListModeloAdapter(getActivity().getBaseContext(),listModelos);
				
				modelos.setAdapter(modeloAdapter);
				
				String params = getActivity().getIntent().getStringExtra("taxi");
				
				if(params!=null){
					try {
						taxi = (TaxiModel) new TaxiModel().toObject(new JSONObject(params));			
						carregarValores(taxi);
					
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
				
				if (taxi != null && valor_carregado == false) {
					carregarValores(taxi);
				}
			}
		}
	}

	public void carregarValores(TaxiModel taxi) {
		
		int position_marca = marcaAdapter.getIndexByID(taxi.getModelo().getMarca().getId());
	
		int position_modelo = modeloAdapter.getIndexByID(taxi.getModelo().getId());
	
		marcas.setSelection(position_marca);
	
		modelos.setSelection(position_modelo);
	
		placa.setText(taxi.getPlaca());
	
		ano.setText(String.valueOf(taxi.getAno()));
	
		if (taxi.getPrincipal() > 0) {
			principal.setChecked(true);
		}
	
		if (taxi.getCom_ar() > 0) {
			comAr.setChecked(true);
		}
		
		valor_carregado = true;	
	}
}
