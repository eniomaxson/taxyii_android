package br.taxyii.library.fragment;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import br.taxyii.library.R;
import br.taxyii.library.dao.UsuarioDAO;
import br.taxyii.library.model.UsuarioModel;
import br.taxyii.library.utils.AndroidUtils;
import br.taxyii.library.utils.IOUtils;
import br.taxyii.library.utils.ImageUtils;
import br.taxyii.library.utils.SDCardUtils;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

public class PerfilUsuarioFragment extends TaxyiiFragment {

	private TextView nome;
	private EditText edt_sobrenome;
	private EditText edt_nome;
	private Button btn_edt_nome;
	private Button btn_ok_nome;

	private TextView sexo;
	private Spinner sp_sexo;
	private Button btn_edt_sexo;
	private Button btn_ok_sexo;
	
	private TextView email;
	private EditText edt_email;
	private Button btn_edt_email;
	private Button btn_ok_email;
	
	private TextView celular;
	private EditText edt_ddd;
	private EditText edt_celular;
	private Button btn_edt_celualr;
	private Button btn_ok_celular;
	
	private ImageView foto_perfil;
	private String foto_dir = "taxyii";
	private String foto_name = "user.jpg";

	private int CAMERA = 1;
	private int GALERIA = 2;
	private int ALTERAR_SENHA = 3;
	
	
	private UsuarioModel pass;
	private String response = null;
	private String operacao = "editar";
	private File file;
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
		formView = getView().findViewById(R.id.viewPrincipal);
		
		statusView = getView().findViewById(R.id.pgBar);
		
		operacao = "listar";
		
		startTransacao();
		
		setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

		View v = inflater.inflate(R.layout.fragment_perfil, null);
		
		ArrayList<String> sexos = new ArrayList<String>();

		sexos.add("Masculino");
		sexos.add("Feminino");

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getSherlockActivity(),android.R.layout.simple_spinner_item, sexos);

		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		
		foto_perfil = (ImageView) v.findViewById(R.id.foto_perfil);
		nome = (TextView) v.findViewById(R.id.nome);
		edt_nome = (EditText) v.findViewById(R.id.edt_nome);
		edt_sobrenome = (EditText) v.findViewById(R.id.edt_sobrenome);
		sexo = (TextView) v.findViewById(R.id.sexo);
		sp_sexo =(Spinner) v.findViewById(R.id.sp_sexo);
		email = (TextView) v.findViewById(R.id.email);
		edt_email = (EditText) v.findViewById(R.id.edt_email);
		celular = (TextView) v.findViewById(R.id.celular);
		edt_celular = (EditText) v.findViewById(R.id.edt_celular);
		edt_ddd = (EditText) v.findViewById(R.id.edt_ddd);
		btn_edt_nome = (Button) v.findViewById(R.id.btn_edt_nome);
		btn_ok_nome = (Button) v.findViewById(R.id.btn_ok_nome);
		btn_edt_sexo = (Button) v.findViewById(R.id.btn_edt_sexo);
		btn_ok_sexo = (Button) v.findViewById(R.id.btn_ok_sexo);
		btn_edt_email= (Button) v.findViewById(R.id.btn_edt_email);
		btn_ok_email= (Button) v.findViewById(R.id.btn_ok_email);
		btn_edt_celualr = (Button) v.findViewById(R.id.btn_edt_celular);
		btn_ok_celular  = (Button) v.findViewById(R.id.btn_ok_celular);
		
		sp_sexo.setAdapter(adapter);
		
		btn_edt_nome.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				exibirFrmEdicao("nome", 1);
			}
		});

		btn_ok_nome.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				operacao = "editar";
				confirmarEdicao("nome");
			}
		});
		
		btn_edt_sexo.setOnClickListener(new OnClickListener() {	
			@Override
			public void onClick(View v) {
				exibirFrmEdicao("sexo", 1);
			}
		});
		
		btn_ok_sexo.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				operacao="editar";
				confirmarEdicao("sexo");
			}
		});
		
		btn_edt_email.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				exibirFrmEdicao("email", 1);
			}
		});
		
		btn_ok_email.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				operacao="editar";
				confirmarEdicao("email");
			}
		});
		
		btn_edt_celualr.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				exibirFrmEdicao("celular", 1);
			}
		});
		
		btn_ok_celular.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				operacao="editar";
				confirmarEdicao("celular");
			}
		});
		
		registerForContextMenu(v.findViewById(R.id.foto_perfil));
		
		return v;
	}

	private void confirmarEdicao(String campo) {

		if (campo.equals("nome")) {
			if ((!pass.getNome().equals(edt_nome.getText().toString())
				|| 
				!pass.getSobrenome().equals(edt_sobrenome.getText().toString()))  && 				
				
				(!edt_nome.getText().toString().equals("")
				&& 
				!edt_sobrenome.getText().toString().equals(""))
				) 
			{
				pass.setNome(edt_nome.getText().toString());
				pass.setSobrenome(edt_sobrenome.getText().toString());
				
				startTransacao();
			}
			
			exibirFrmEdicao("nome", 0);
		}
		
		if(campo.equals("sexo")){
			String sexo_selecionado = sp_sexo.getSelectedItem().toString().equals("Masculino") ? "M":"F" ;
			
			if(!pass.getSexoChar().equals(sexo_selecionado)){
				pass.setSexo(sexo_selecionado);
				startTransacao();
			}
			exibirFrmEdicao("sexo", 0);
		}
		
		if(campo.equals("email")){
			if(!pass.getEmail().equals(edt_email.getText().toString()) && !edt_email.getText().toString().equals("")){
				pass.setEmail(edt_email.getText().toString());
				startTransacao();
			}
			exibirFrmEdicao("email", 0);
		}
		if(campo.equals("celular")){
			
			String celular = edt_ddd.getText().toString() + edt_celular.getText().toString();
			
			if(!pass.getCelular().equals(celular) && !edt_celular.getText().toString().equals("")){
				pass.setCelular(celular);
				startTransacao();
			}
			exibirFrmEdicao("celular", 0);
		}
	}

	private void exibirFrmEdicao(String frm, int opcao) {
		View view = null;
		View form = null;
		
		if (frm.equals("nome")) {
			view = getView().findViewById(R.id.view_nome);
			form = getView().findViewById(R.id.view_frm_nome);
		}else if(frm.equals("sexo")){
			view = getView().findViewById(R.id.view_sexo);
			form = getView().findViewById(R.id.view_frm_sexo);
		}else if(frm.equals("email")){
			view = getView().findViewById(R.id.view_email);
			form = getView().findViewById(R.id.view_frm_email);			
		}else{
			view = getView().findViewById(R.id.view_celular);
			form = getView().findViewById(R.id.view_frm_celular);			
		}
		
		if (opcao > 0) {
			view.setVisibility(View.GONE);
			form.setVisibility(View.VISIBLE);
		} else {
			view.setVisibility(View.VISIBLE);
			form.setVisibility(View.GONE);
		}
	
	}
	
	public void setDadosUsuario(){		
		nome.setText(pass.getNome() + " " + pass.getSobrenome());
		sexo.setText(pass.getSexo());
		email.setText(pass.getEmail());
		
		celular.setText("(" + pass.getCelular().subSequence(0, 2)+ ")" + " " + pass.getCelular().substring(2) );

		edt_nome.setText(pass.getNome());
		edt_sobrenome.setText(pass.getSobrenome());
		edt_email.setText(pass.getEmail());
		
		edt_ddd.setText(pass.getCelular().substring(0, 2));
		edt_celular.setText(pass.getCelular().substring(2));
		
		File file = SDCardUtils.getSdCardFile(foto_dir, foto_name);
		try{
			Bitmap bitmap = ImageUtils.getResizedImage(Uri.fromFile(file), 210, 230);			
			if (bitmap != null)
				foto_perfil.setImageBitmap(bitmap);
		}catch(Exception e){
			
		}
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// TODO Auto-generated method stub
		super.onCreateOptionsMenu(menu, inflater);
		menu.add(0, ALTERAR_SENHA, 0, "Alterar Senha").setIcon(R.drawable.ic_action_key_light).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int opcao = item.getItemId();
		
		if (opcao == ALTERAR_SENHA) {

		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void executar() throws Exception {
		
		if(operacao.equals("editar")){
			response = pass.alterar();
		}else{
			pass = new UsuarioDAO(getActivity().getBaseContext()).get();
			response = pass.getPassageiro(pass.getId());
		}
	}

	@Override
	public void atualizarView() {
		if (response != null) {
			if (operacao.equals("editar")) {
				if (!response.contains("error")) {
					JSONObject json;
					try {
						json = new JSONObject(response);
						pass = (UsuarioModel) new UsuarioModel().toObject(json);
						setDadosUsuario();
					} catch (JSONException e) {
						e.printStackTrace();
					}					
					new UsuarioDAO(getActivity().getBaseContext()).atualizar(pass);
					AndroidUtils.showSimpleToast(getActivity(),R.string.msgConfirmacao, Toast.LENGTH_SHORT);
				}else{
					try {
						JSONObject json = new JSONObject(response).getJSONObject("error");
						if(json.has("email")){
							Toast.makeText(getActivity().getBaseContext(), getString(R.string.error_email_cadastrado), Toast.LENGTH_SHORT).show();
							pass.setEmail(email.getText().toString());
							edt_email.setText(pass.getEmail());
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			} else {
				try {
					JSONObject json = new JSONObject(response);
					pass = (UsuarioModel) new UsuarioModel().toObject(json);
					setDadosUsuario();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		menu.setHeaderTitle("Selecione uma imagem"); 
		menu.add(android.view.Menu.NONE,CAMERA,android.view.Menu.NONE,"Camera").setIcon(android.R.drawable.ic_menu_camera);
		menu.add(android.view.Menu.NONE,GALERIA,android.view.Menu.NONE,"Galeria").setIcon(android.R.drawable.ic_menu_gallery);
	}
	
	@Override
	public boolean onContextItemSelected(android.view.MenuItem item) {

		int opcao = item.getItemId();
		
		file = SDCardUtils.getSdCardFile(foto_dir, foto_name);
		
		if(opcao == CAMERA){
			Intent i = new Intent("android.media.action.IMAGE_CAPTURE");
			i.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
			startActivityForResult(i, CAMERA);
		}
		
		if(opcao == GALERIA){
			Intent i = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
			startActivityForResult(i, GALERIA);
		}
		
		return super.onContextItemSelected(item);
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);		
		if (resultCode == getActivity().RESULT_OK) {
			if (requestCode == CAMERA) {
				Bitmap bitmap = ImageUtils.getResizedImage(Uri.fromFile(SDCardUtils.getSdCardFile(foto_dir, foto_name)), 300, 380);
				foto_perfil.setImageBitmap(bitmap);
			}
			if (requestCode == GALERIA) {

				Uri selectedImage = data.getData();
				InputStream imageStream;
				try {
					imageStream = getActivity().getContentResolver().openInputStream(selectedImage);

					file = SDCardUtils.getSdCardFile(foto_dir, foto_name);

					SDCardUtils.writeToSdCard(file,IOUtils.toBytes(imageStream));

					Bitmap bitmap = ImageUtils.getResizedImage(Uri.fromFile(file), 300, 380);

					foto_perfil.setImageBitmap(bitmap);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
