package br.taxyii.library.fragment;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import br.taxyii.library.R;
import br.taxyii.library.dao.UsuarioDAO;
import br.taxyii.library.model.CorridaModel;
import br.taxyii.library.model.LocalizacaoModel;
import br.taxyii.library.model.TaxistaModel;
import br.taxyii.library.model.UsuarioModel;
import br.taxyii.library.utils.SharedPreferencesUtils;


public class AceitarCorridaFragment extends TaxyiiFragment {

	private TextView passagiro_nome, passageiro_celular, bairro, rua, numero,ponto_referencia;
	private Button btnAceitar, btnNegar, btnFinalizar;
	private double longitude_destino, latitude_destino, latitude_origem, longitude_origem;
	private boolean corrida_aceita = false;
	private String response;
	private LocalizacaoModel localizacao;
	private String operacao; 
	private CorridaModel corrida;
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		localizacao = new LocalizacaoModel(getActivity().getBaseContext(), this, 4);
		loadParams();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		
		View view = inflater.inflate(R.layout.fragment_aceitar_pedido, null);
		
		formView = (View) view.findViewById(R.id.frmAceitarPedido);
		statusView = (View) view.findViewById(R.id.pgBar);
		
		passageiro_celular = (TextView) view.findViewById(R.id.passageiro_celular);
		passagiro_nome = (TextView) view.findViewById(R.id.passageiro_nome);
		bairro = (TextView) view.findViewById(R.id.bairro);
		rua = (TextView) view.findViewById(R.id.rua);
		numero = (TextView) view.findViewById(R.id.numero);
		ponto_referencia = (TextView) view.findViewById(R.id.ponto_referencia);
		btnAceitar = (Button) view.findViewById(R.id.btnAceitar);
		btnNegar = (Button) view.findViewById(R.id.btnNegar);
		btnFinalizar = (Button) view.findViewById(R.id.btnFinalizar);
		
		btnFinalizar.setVisibility(View.INVISIBLE);
		
		btnAceitar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {	
				operacao = "aceitar";
				startTransacao();	
			}
		});
		
		setRetainInstance(true);
		
		return view;
	}
	
	public void loadParams() {
		SharedPreferencesUtils utils = new SharedPreferencesUtils(getActivity().getBaseContext(), SharedPreferencesUtils.CORRIDA_PREFERENCES);
		
		JSONObject param_json;
		try {
			
			String dado_corrida = utils.getPreferences(SharedPreferencesUtils.CORRIDA_ATUAL, null);
			Log.i("dados", dado_corrida);
			param_json = new JSONObject(dado_corrida);
			
			corrida = (CorridaModel) new CorridaModel().toObject(param_json);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		passagiro_nome.setText(corrida.getPassageiro().getNome());
		passageiro_celular.setText(corrida.getPassageiro().getCelular());
		
		rua.setText(corrida.getEndereco().getRua());
		bairro.setText(corrida.getEndereco().getBairro());
		numero.setText(corrida.getEndereco().getNumero());
		ponto_referencia.setText(corrida.getEndereco().getPonto_referencia());
		
		latitude_destino = Double.valueOf(corrida.getEndereco().getCoordenada().split(",")[0]);
		longitude_destino = Double.valueOf(corrida.getEndereco().getCoordenada().split(",")[1]);	
	}
	
	@Override
	public void executar() throws Exception {
		if(operacao.equals("aceitar")){
			aceitarCorrida();
		}else if(operacao.equals("negar")){
			recusarCorrida();
		}else{
			finalizarCorrida();
		}
	}
	
	@Override
	public void atualizarView() {
		
		if(response != null){
			if(operacao.equals("aceitar")){
				if(!response.contains("error")){
					//Fazer o procedimento aqui quando aceitar ocorrer bem
					new TaxistaModel().alterarStatusTaxi(getActivity().getBaseContext(), true);
					
					corrida_aceita = true;
					
					String uri = "http://maps.google.com/maps?f=d&saddr=" + latitude_origem + "," + longitude_origem + "&daddr="+ latitude_destino + "," + longitude_destino +"&h1=pt";
					
					startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(uri)));
				}
				
			}else if(operacao.equals("negar")){
				
				if(!response.contains("error")){
					//Fazer procedimento aqui quando negar não ocorreu erros
				}
				
			}else{
				
			}
			
		}else{
			
		}
	}
	
	// Resposta para o passageiro com o ok
	public void aceitarCorrida(){
		UsuarioModel usuario = new UsuarioDAO(getActivity().getBaseContext()).get();
		response = new TaxistaModel().aceitarCorrida(corrida, usuario.getId());
	}
	
	// Resposta para o sistema reprocessar o pedido do passageiro
	public void recusarCorrida(){
		UsuarioModel usuario = new UsuarioDAO(getActivity().getBaseContext()).get();
		response = new TaxistaModel().negarCorrida(corrida, usuario.getId());
	}
	
	// Resposta para o sistema com a confirmação da corrida finalizada
	public void finalizarCorrida(){
		new TaxistaModel().alterarStatusTaxi(getActivity().getBaseContext(), false);
	}
}
