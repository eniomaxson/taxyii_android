package br.taxyii.library.fragment;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;
import br.taxyii.library.R;
import br.taxyii.library.adapter.ListCarroAdper;
import br.taxyii.library.faces.ITransacao;
import br.taxyii.library.model.TaxiModel;
import br.taxyii.library.task.DefaultTask;
import br.taxyii.library.utils.AndroidUtils;

import com.actionbarsherlock.view.ActionMode;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

public class ListaTaxiFragment extends TaxyiiFragment{
	
	private int ADD = 1;
	private String response = null;
	private ListCarroAdper taxiAdapter;
	private ArrayList<TaxiModel> veiculos;
	private DefaultTask task;
	private ActionMode mMode;
	private TaxiModel taxiSelecionado;
	private int EDIT = 1;
	private int REMOVE = 2;
	private int ADD_ACESSORIO = 3;
	private ListView taxis;
	private ProgressFragment progress;
	private View frmList;
	private ActionMode action;
	
	private String acao;
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
		startTransacao();
		
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_list_taxi, null);
		
		frmList = v.findViewById(R.id.frmListTaxi);
		
		taxis = (ListView) v.findViewById(R.id.lisViewtTaxi);
		
		setRetainInstance(true);
		
		return v;
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		menu.add(0, ADD, 0, "Adicionar").setIcon(android.R.drawable.ic_menu_add).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		int id = item.getItemId();

		if (id == ADD) {
			startActivity(new Intent(acao));
		}
		
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void executar() throws Exception {
		response = new TaxiModel().listar(getActivity().getBaseContext());
	}
	
	@Override
	public void atualizarView() {
		if (response != null) {
			if (!response.contains("error")) {
				try {
					veiculos = (ArrayList<TaxiModel>) new TaxiModel().toCollectionObject(new JSONArray(response));
					taxiAdapter = new ListCarroAdper(getActivity().getBaseContext(), veiculos);
					taxis.setAdapter(taxiAdapter);
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	private class Tremove implements ITransacao {

		@Override
		public void executar() throws Exception {
			response = taxiSelecionado.remover();
		}

		@Override
		public void atualizarView() {
			if (response != null) {
				veiculos.remove(taxiSelecionado);
				taxis.setAdapter(taxiAdapter);
				AndroidUtils.showSimpleToast(getActivity(),1, Toast.LENGTH_SHORT);
			}
		}

	}

	private String getAcao() {
		return acao;
	}

	private void setAcao(String acao) {
		this.acao = acao;
	}

	

}
