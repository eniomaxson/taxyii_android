package br.taxyii.library.model;

import org.json.JSONObject;

import com.google.android.gms.maps.model.LatLng;

public class PedidoModel extends Model {
	private TaxistaModel taxista;
	private UsuarioModel passageiro;
	private LatLng pontoPartida;
	private String status;

	@Override
	public String toJson() {
		// TODO Auto-generated method stub
		return super.toJson();
	}

	@Override
	public Object toObject(JSONObject jsonObject) {
		// TODO Auto-generated method stub
		return super.toObject(jsonObject);
	}

	public TaxistaModel getTaxista() {
		return taxista;
	}

	public void setTaxista(TaxistaModel taxista) {
		this.taxista = taxista;
	}

	public UsuarioModel getPassageiro() {
		return passageiro;
	}

	public void setPassageiro(UsuarioModel passageiro) {
		this.passageiro = passageiro;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the pontoPartida
	 */
	public LatLng getPontoPartida() {
		return pontoPartida;
	}

	/**
	 * @param pontoPartida the pontoPartida to set
	 */
	public void setPontoPartida(LatLng pontoPartida) {
		this.pontoPartida = pontoPartida;
	}
}
