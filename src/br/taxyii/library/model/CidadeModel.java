package br.taxyii.library.model;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import br.taxyii.library.faces.IJson;

public class CidadeModel implements IJson {
	private int id;
	private String descricao;
	public ArrayList<CidadeModel> cidades;

	public CidadeModel(String descricao) {
		this.descricao = descricao;
	}

	public CidadeModel(int id){
		this.id = id;
	}
	
	public CidadeModel() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public Object toObject(JSONObject jsonObject) {
		CidadeModel cidade = new CidadeModel();
		try {
			cidade.setId(jsonObject.getInt("id"));
			cidade.setDescricao(jsonObject.getString("descricao"));
			return cidade;
		} catch (JSONException e) {
			return null;
		}
	}

	@Override
	public String toJson() {
		JSONObject json = new JSONObject();
		try {
			json.put("id", getId()).put("descricao", descricao);
			return json.toString();
		} catch (JSONException e) {
			return null;
		}
	}

	@Override
	public Object toCollectionObject(JSONArray jsonArray) {
		return null;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}
