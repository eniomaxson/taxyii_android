package br.taxyii.library.model;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import br.taxyii.library.dao.UsuarioDAO;
import br.taxyii.library.faces.IJson;
import br.taxyii.library.utils.HttpClientUtils;


public class TaxiModel extends Model implements IJson {
	private int id;
	private String placa;
	private int ano;
	private TaxistaModel taxista;
	private ModeloModel modelo;
	private ArrayList<AcessorioModel> acessorios;
	private HttpClientUtils http;
	private int principal;
	private int com_ar;

	public int getCom_ar() {
		return com_ar;
	}

	public void setCom_ar(int com_ar) {
		this.com_ar = com_ar;
	}

	public int getPrincipal() {
		return principal;
	}

	public void setPrincipal(int principal) {
		this.principal = principal;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public TaxistaModel getTaxista() {
		return taxista;
	}

	public void setTaxista(TaxistaModel taxista) {
		this.taxista = taxista;
	}

	public ModeloModel getModelo() {
		return modelo;
	}

	public void setModelo(ModeloModel modelo) {
		this.modelo = modelo;
	}

	public ArrayList<AcessorioModel> getAcessorios() {
		return acessorios;
	}

	public void setAcessorios(ArrayList<AcessorioModel> acessorios) {
		this.acessorios = acessorios;
	}

	@Override
	public Object toObject(JSONObject jsonObject) {

		TaxiModel taxi = new TaxiModel();
		ModeloModel modelo = new ModeloModel();
		MarcaModel marca = new MarcaModel();

		try {
			marca.setDescricao(jsonObject.getString("marca"));
			marca.setId(jsonObject.getInt("marca_id"));
			modelo.setDescricao(jsonObject.getString("modelo"));
			modelo.setId(jsonObject.getInt("taxi_modelo_id"));
			taxi.setAno(jsonObject.getInt("ano"));
			taxi.setId(jsonObject.getInt("id"));
			taxi.setPrincipal(jsonObject.getInt("principal"));
			taxi.setPlaca(jsonObject.getString("placa"));
			taxi.setCom_ar(jsonObject.getInt("com_ar"));
			taxi.setTaxista(new TaxistaModel(jsonObject.getInt("taxista_id")));
			

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		modelo.setMarca(marca);
		taxi.setModelo(modelo);

		return taxi;
	}

	@Override
	public String toJson() {
		JSONObject json = new JSONObject();
		try {
			if (id > 0) {
				json.put("id", id);
			}
			json.put("placa", placa).put("ano", ano)
					.put("taxi_modelo_id", getModelo().getId());
			json.put("principal", principal);
			json.put("com_ar", getCom_ar());	
			if (getModelo().getDescricao() != null) {
				json.put("modelo", getModelo().getDescricao());
			}
			if (getModelo().getMarca() != null) {
				json.put("marca", getModelo().getMarca().getDescricao());
				json.put("marca_id", getModelo().getMarca().getId());
			}
			if (getTaxista().getId() > 0) {
				json.put("taxista_id", getTaxista().getId());
			} else {
				json.put("taxista_id", getTaxista().getUsuario().getId());
			}
		} catch (JSONException e) {
			return null;
		}
		return json.toString();
	}

	@Override
	public Object toCollectionObject(JSONArray jsonArray) {
		ArrayList<TaxiModel> taxis = new ArrayList<TaxiModel>();

		for (int i = 0; i < jsonArray.length(); i++) {
			try {
				JSONObject j = jsonArray.getJSONObject(i);
				taxis.add((TaxiModel) toObject(j));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return taxis;
	}

	public int getAno() {
		return ano;
	}

	public void setAno(int ano) {
		this.ano = ano;
	}

	public String listar(Context ctx) {
		UsuarioModel user = new UsuarioDAO(ctx).get();

		String url = URL_PREFIX + "taxi/listTaxiToTaxista?idUsuario="+ user.getId();

		http = new HttpClientUtils(url);
		try {
			return http.requisicaoGetJson();
		} catch (Exception e) {
			return null;
		}
	}

	public String cadastrar() {
		http = new HttpClientUtils(URL_PREFIX + "taxi/create");
		try {
			return http.requisicaoPostJson(toJson());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}

	public String remover() {
		http = new HttpClientUtils(URL_PREFIX + "taxi/delete?id=" + id);
		try {
			return http.requisicaoGetJson();
		} catch (Exception e) {
			return null;
		}

	}

	public String alterar() {
		http = new HttpClientUtils(URL_PREFIX + "taxi/update?id=" + id);
		try {
			return http.requisicaoPostJson(toJson());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
