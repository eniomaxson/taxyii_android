package br.taxyii.library.model;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import br.taxyii.library.dao.UsuarioDAO;
import br.taxyii.library.utils.HttpClientUtils;


public class EnderecoModel extends Model {

	private String rua;
	private String bairro;
	private String cidade;
	private String coordenada;
	private String ponto_referencia;
	private String numero;
	private int id;
	private String nome;
	private UsuarioModel pass;
	private HttpClientUtils http;
	
	
	public EnderecoModel(String coordenada, String ponto_referencia,
			String numero, UsuarioModel passageiro) {
		this.coordenada = coordenada;
		this.ponto_referencia = ponto_referencia;
		this.numero = numero;
		this.pass = passageiro;
	}

	public EnderecoModel() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toJson() {
		JSONObject json = new JSONObject();
		try {
			json.put("rua", rua == null  ? "" : getRua());
			json.put("bairro", bairro == null ? "": getBairro());
			json.put("coordenada", getCoordenada());
			json.put("numero", numero == null  ? "" : getNumero());
			json.put("nome", nome == null  ? "" : getNome());
			json.put("ponto_referencia", ponto_referencia == null ? "" : getPonto_referencia());
			json.put("cidade", cidade == null ? "" : getCidade());
			
			if(pass != null)
				json.put("passageiro_id", pass.getId());
			
			if (id > 0) {
				json.put("id", id);
			}

		} catch (JSONException e) {
			return null;
		}
		return json.toString();
	}

	@Override
	public Object toObject(JSONObject jsonObject) {

		EnderecoModel end = new EnderecoModel();
		try {
			end.setId(jsonObject.has("id") ? jsonObject.getInt("id") : 0);
			end.setNome(jsonObject.has("nome") ? jsonObject.getString("nome"): "");
			end.setRua(jsonObject.has("rua") ? jsonObject.getString("rua"): "");			
			end.setBairro(jsonObject.has("bairro") ? jsonObject.getString("bairro") : "");
			end.setCidade(jsonObject.has("cidade") ? jsonObject.getString("cidade"): "");
			end.setNumero(jsonObject.has("numero") ? jsonObject.getString("numero") : "");
			end.setPonto_referencia(jsonObject.has("ponto_referencia")? jsonObject.getString("ponto_referencia"):"");
			end.setCoordenada(jsonObject.getString("coordenada"));
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}

		return end;
	}

	@Override
	public Object toCollectionObject(JSONArray jsonArray) {

		ArrayList<EnderecoModel> enderecos = new ArrayList<EnderecoModel>();
		try {
			for (int i = 0; i < jsonArray.length(); i++) {
				enderecos.add((EnderecoModel) toObject(jsonArray.getJSONObject(i)));
			}
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
		return enderecos;
	}

	public JSONArray toJsonArray(ArrayList<EnderecoModel> enderecos){
		
		JSONArray jsonArray = new JSONArray();
		
		for (EnderecoModel enderecoModel : enderecos) {
			
			try {
				jsonArray.put(new JSONObject(enderecoModel.toJson()));
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		
		return jsonArray;
	}
	
/*	public List<Address> getEnderecoFromCoordenada(Context context,double latitude, double longitude) {
		List<Address> endereco = null;

		Geocoder geocoder = new Geocoder(context, Locale.getDefault());
		try {
			endereco = geocoder.getFromLocation(latitude, longitude, 1);
		} catch (IOException e) {
			return null;
		}
		return endereco;
	}*/

	public String listEndereco(Context ctx) {
		int id = new UsuarioDAO(ctx).get().getId();
		http = new HttpClientUtils(URL_PREFIX
				+ "enderecoFavorito/list?passageiro_id=" + id);
		try {
			return http.requisicaoGetJson();
		} catch (Exception e) {
			return null;
		}
	}

	public String cadastrar() {
		http = new HttpClientUtils(URL_PREFIX + "enderecoFavorito/create");
		try {
			return http.requisicaoPostJson(toJson());
		} catch (Exception e) {
			return null;
		}
	}

	public String alterar() {
		http = new HttpClientUtils(URL_PREFIX + "enderecoFavorito/update");
		try {
			return http.requisicaoPostJson(toJson());
		} catch (Exception e) {
			return null;
		}
	}

	public String deletar() {
		http = new HttpClientUtils(URL_PREFIX + "enderecoFavorito/delete?id="
				+ id);
		try {
			return http.requisicaoGetJson();
		} catch (Exception e) {
			return null;
		}
	}

	public String getRua() {
		return rua;
	}

	public void setRua(String rua) {
		this.rua = rua;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getCoordenada() {
		return coordenada;
	}

	public void setCoordenada(String coordenada) {
		this.coordenada = coordenada;
	}

	public String getPonto_referencia() {
		return ponto_referencia;
	}

	public void setPonto_referencia(String ponto_referencia) {
		this.ponto_referencia = ponto_referencia;
	}

	/**
	 * @return the pass
	 */
	public UsuarioModel getPass() {
		return pass;
	}

	/**
	 * @param pass
	 *            the pass to set
	 */
	public void setPass(UsuarioModel pass) {
		this.pass = pass;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @param nome
	 *            the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

}
