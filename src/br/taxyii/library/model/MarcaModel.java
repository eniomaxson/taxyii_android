package br.taxyii.library.model;

import java.util.ArrayList;

import android.content.Context;
import br.taxyii.library.dao.MarcaDAO;


public class MarcaModel {
	private int id;
	private String descricao;

	public MarcaModel(int marcaId) {
		this.id = marcaId;
	}

	public MarcaModel() {
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public ArrayList<MarcaModel> listMarca(Context ctx) {

		ArrayList<MarcaModel> marcas = new MarcaDAO(ctx).list();

		return marcas;
	}

}
