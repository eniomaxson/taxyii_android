package br.taxyii.library.model;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationRequest;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import br.taxyii.library.fragment.TaxyiiFragment;
import br.taxyii.library.utils.LocationUtils;

public class LocalizacaoModel implements ConnectionCallbacks,OnConnectionFailedListener,com.google.android.gms.location.LocationListener {

	private LocationRequest mLocationRequest;
	private LocationClient mLocationClient;
	private boolean mUpdatesRequested = false;
	private Context context;
	private Location current_location;
	private TaxyiiFragment fragment;
	private int qtde_atualizacao;
	
	public LocalizacaoModel(Context context, TaxyiiFragment fragment, int qtde_atualizacao) {

		this.fragment = fragment;
		
		this.context = context;
		
		this.qtde_atualizacao = qtde_atualizacao;
		
		mLocationRequest = LocationRequest.create();
		
		// Set the update interval 
		mLocationRequest.setInterval(LocationUtils.UPDATE_INTERVAL_IN_MILLISECONDS);

		// Use high accuracy
		mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

		// Set the interval ceiling to one minute
		mLocationRequest.setFastestInterval(LocationUtils.FAST_INTERVAL_CEILING_IN_MILLISECONDS);
		
		// Note that location updates are off until the user turns them on
		setmUpdatesRequested(true);
		
		if(qtde_atualizacao > 0)
			mLocationRequest.setNumUpdates(qtde_atualizacao);
		
		setmLocationClient(new LocationClient(context, this, this));
		
		mLocationClient.connect();
	}
	
	public void start()
	{
		startPeriodicUpdates();
	}
	
	public void stop(){
		if (mLocationClient.isConnected()) {
			stopPeriodicUpdates();
		}
	}
	
	private void startPeriodicUpdates() {
		mUpdatesRequested = true;
		mLocationClient.requestLocationUpdates(mLocationRequest, this);
	}
	
	private void stopPeriodicUpdates() {
		mUpdatesRequested = false;
		mLocationClient.removeLocationUpdates(this);
	}
	
	public void atualizarView(){
		fragment.startTransacao();
	}
	@Override
	public void onLocationChanged(Location location) {
		setCurrent_location(location);
	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {

	}

	@Override
	public void onConnected(Bundle connectionHint) {
		if(mUpdatesRequested == true)
			startPeriodicUpdates();
	}

	@Override
	public void onDisconnected() {
		// TODO Auto-generated method stub
	}

	public LocationClient getmLocationClient() {
		return mLocationClient;
	}

	public void setmLocationClient(LocationClient mLocationClient) {
		this.mLocationClient = mLocationClient;
	}

	public boolean ismUpdatesRequested() {
		return mUpdatesRequested;
	}

	public void setmUpdatesRequested(boolean mUpdatesRequested) {
		this.mUpdatesRequested = mUpdatesRequested;
	}

	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}

	public Location getCurrent_location() {
		return current_location;
	}

	public void setCurrent_location(Location current_location) {
		this.current_location = current_location;
	}

	public int getQtde_atualizacao() {
		return qtde_atualizacao;
	}

	public void setQtde_atualizacao(int qtde_atualizacao) {
		this.qtde_atualizacao = qtde_atualizacao;
	}
}
