package br.taxyii.library.model;

import java.util.ArrayList;

import android.content.Context;
import br.taxyii.library.dao.ModeloDAO;


public class ModeloModel  {
	private int id;
	private String descricao;
	private MarcaModel marca;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public MarcaModel getMarca() {
		return marca;
	}

	public void setMarca(MarcaModel marca) {
		this.marca = marca;
	}

	public ArrayList<ModeloModel> listModelo(Context ctx, long marca_id) {
		ArrayList<ModeloModel> modelos = new ModeloDAO(ctx).list(marca_id);
		return modelos;
	}

	public ModeloModel(int modeloId, int marcaId) {
		this.id = modeloId;
		this.marca = new MarcaModel(marcaId);
	}

	public ModeloModel() {
		// TODO Auto-generated constructor stub
	}
}
