package br.taxyii.library.model;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import br.taxyii.library.dao.UsuarioDAO;
import br.taxyii.library.faces.IJson;
import br.taxyii.library.utils.HttpClientUtils;


public class FormaPagamentoModel extends Model implements IJson {

	private int id;
	private String descricao;
	private int aceita;
	private int taxista_id; // para identificar o motorista
	private HttpClientUtils http;

	public int getTaxista_id() {
		return taxista_id;
	}

	public void setTaxista_id(int taxista_id) {
		this.taxista_id = taxista_id;
	}

	@Override
	public Object toObject(JSONObject jsonObject) {

		FormaPagamentoModel frmPgt = new FormaPagamentoModel();
		try {

			frmPgt.setAceita(jsonObject.getInt("aceita"));
			frmPgt.setId(jsonObject.getInt("id"));
			frmPgt.setDescricao(jsonObject.getString("descricao"));

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return frmPgt;
	}

	@Override
	public String toJson() {
		JSONObject j = new JSONObject();

		try {
			j.put("id", id).put("descricao", descricao).put("aceita", aceita);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return j.toString();
	}

	@Override
	public Object toCollectionObject(JSONArray jsonArray) {
		ArrayList<FormaPagamentoModel> formasPagametos = new ArrayList<FormaPagamentoModel>();

		try {
			for (int i = 0; i < jsonArray.length(); i++) {
				formasPagametos.add((FormaPagamentoModel) toObject(jsonArray
						.getJSONObject(i)));
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return formasPagametos;
	}

	public String listFormaPagamento(Context ctx) {
		int id = new UsuarioDAO(ctx).get().getId();
		http = new HttpClientUtils(URL_PREFIX
				+ "formaPagamento/list?usuario_id=" + id);
		try {
			return http.requisicaoGetJson();
		} catch (Exception e) {
			return null;
		}
	}

	public String atualizaFormaPagamento(Context ctx,
			ArrayList<FormaPagamentoModel> formas_pagamentos_aceitas) {
		String stringJson = "[";
		for (FormaPagamentoModel f : formas_pagamentos_aceitas) {
			stringJson += f.toJson() + ",";
		}
		stringJson = stringJson.substring(0, stringJson.length() - 1);
		stringJson += "]";
		JSONArray json = null;

		try {
			json = new JSONArray(stringJson);

			int id = new UsuarioDAO(ctx).get().getId();

			http = new HttpClientUtils(URL_PREFIX
					+ "formaPagamento/update?usuario_id=" + id);

			return http.requisicaoPostJson(json.toString());
		} catch (JSONException e) {
			return null;
		} catch (Exception e) {
			return null;
		}

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public int getAceita() {
		return aceita;
	}

	public void setAceita(int aceita) {
		this.aceita = aceita;
	}
}
