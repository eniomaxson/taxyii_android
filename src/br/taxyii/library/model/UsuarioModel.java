package br.taxyii.library.model;

import java.util.concurrent.atomic.AtomicInteger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import br.taxyii.library.dao.UsuarioDAO;
import br.taxyii.library.utils.HttpClientUtils;
import br.taxyii.library.utils.SharedPreferencesUtils;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.maps.model.LatLng;

public class UsuarioModel extends Model {
	protected int id;
	protected Byte foto;
	protected String nome;
	protected String sobrenome;
	protected String sexo;
	protected String email;
	protected String senha;
	protected String celular;
	protected String bloqueio;
	protected int ultimo_acesso;
	protected LatLng posicao_atual;
	protected String gcm_id;
	GoogleCloudMessaging gcm;
	AtomicInteger msgId = new AtomicInteger();
	public boolean validado = true;
	public CidadeModel cidade;
	private HttpClientUtils http;
	private UsuarioDAO dao;

	public UsuarioModel(String nome, String sobrenome, String sexo,
			String email, String senha, String celular) {
		this.nome = nome;
		this.sobrenome = sobrenome;
		this.setSexo(sexo);
		this.email = email;
		this.senha = senha;
		this.celular = celular;
	}

	public UsuarioModel() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Object toObject(JSONObject json) {
		UsuarioModel passageiro = new UsuarioModel();
		try {
			if (json.has("id"))
				passageiro.setId(json.getInt("id"));
			
			passageiro.setNome(json.getString("nome"));
			passageiro.setSobrenome(json.getString("sobrenome"));
			passageiro.setEmail(json.getString("email"));
			passageiro.setSenha(json.getString("senha"));
			passageiro.setSexo(json.getString("sexo"));
			passageiro.setCelular(json.getString("celular"));

			if (json.has("bloqueio"))
				passageiro.setBloqueio(json.getString("bloqueio"));

			if (getCidade() != null)
				passageiro.setCidade(new CidadeModel(json.getString("descricao_cidade")));

			if (json.has("gcm_device_id"))
				passageiro.setGcmId(json.getString("gcm_device_id"));

			return passageiro;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String toJson() {
		JSONObject json = new JSONObject();
		try {

			json.put("nome", nome).put("sobrenome", sobrenome)
					.put("email", email).put("celular", celular)
					.put("sexo", sexo);
			if (getCidade() != null)
				json.put("descricao_cidade", cidade.getDescricao());

			if (gcm_id != null)
				json.put("gcm_device_id", getGcmId());

			if (senha != null)
				json.put("senha", senha);

			if (bloqueio != null)
				json.put("bloqueio", getBloqueio());

			return json.toString();

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Object toCollectionObject(JSONArray jsonArray) {
		// TODO Auto-generated method stub
		return null;
	}

	public String cadastrar(Activity activity) {
		String response;
		http = new HttpClientUtils(URL_PREFIX + "usuarios");
		try {
			response = http.requisicaoPostJson(toJson());

		} catch (Exception e) {
			return null;
		}
		return response;
	}

	public String alterar() {
		String response;
		http = new HttpClientUtils(URL_PREFIX + "usuario/update?id=" + id);
		try {
			response = http.requisicaoPostJson(toJson());
		} catch (Exception e) {
			return null;
		}
		return response;
	}
	/*
	 * Metodo responsavel por requisitar o registro gmc ao google
	 */
	public void gcmRegister(Context context) {

		String response = null;

		UsuarioDAO dao = new UsuarioDAO(context);

		UsuarioModel usuario = dao.get();

		dao.fechar();

		usuario.setGcmId(getGcmPreference(context));

		try {
			response = usuario.alterar();
		} catch (Exception e) {
			e.printStackTrace();
		}
		setEnviado(response, context);
	}

	// Verificar se o gcm foi enviado

	public String gcmJaEnviado(Context context) {
		String enviado = "S";

		SharedPreferencesUtils utils = new SharedPreferencesUtils(context,
				SharedPreferencesUtils.GCM_PREFERENCES);

		String gcmEnviado = utils.getPreferences(
				SharedPreferencesUtils.GCM_ENVIADO, null);

		if (gcmEnviado == null) {
			return null;
		} else if (gcmEnviado.equals("N")) {
			enviado = "N";
		}

		return enviado;
	}

	// Retorna o gcm id do usuario

	public String getGcmPreference(Context context) {

		SharedPreferencesUtils utils = new SharedPreferencesUtils(context,
				SharedPreferencesUtils.GCM_PREFERENCES);

		String gcmId = utils.getPreferences(
				SharedPreferencesUtils.GCM_REGISTRATION_ID, null);

		return gcmId;
	}

	// Atualiza flag de enviado para verdadeiro

	public void setEnviado(String resposta, Context context) {

		SharedPreferencesUtils util = new SharedPreferencesUtils(context,
				SharedPreferencesUtils.GCM_PREFERENCES);

		try {
			if (resposta != null) {
				UsuarioModel usuario = (UsuarioModel) toObject(new JSONObject(
						resposta));
				if (usuario.getGcmId().length() > 10) {
					util.setPreferences(SharedPreferencesUtils.GCM_ENVIADO, "S");
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public String getUsuarioFromEmail(String email) {
		String url = "usuario/getToMail?email=" + email;

		http = new HttpClientUtils(URL_PREFIX + url);

		try {
			return http.requisicaoGetJson();
		} catch (Exception e) {
			return null;
		}
	}

	public String login(JSONObject login) {
		String url = URL_PREFIX + "acesso/autenticar";
		http = new HttpClientUtils(url);
		try {
			return http.requisicaoPostJson(login.toString());
		} catch (Exception e) {
			return null;
		}
	}

	public String getPassageiro(int id) {
		String response = null;
		
		http = new HttpClientUtils(URL_PREFIX + "usuario/get?id=" + id);
		try {
			
			response = http.requisicaoGetJson();
			
		} catch (Exception e) {
			Log.i("GET",e.getMessage());
		}
		
		return response;
	}
	
	/* 
	 * Metodo responsavel por fazer uma requisiçao para o servidor solicitando uma corrida 
	 */
	public String solitarCorrida(CorridaModel corrida){
		String response = null;
		http = new HttpClientUtils(URL_PREFIX + "corrida/getTaxi");
		try {
			response = http.requisicaoPostJson(corrida.toJson());
		} catch (Exception e) {
			response = null;
		}
		return response;
	}
	
	/* Get and Sets */
	public void setGcmId(String gcmId) {
		this.gcm_id = gcmId;
	}

	public String getGcmId() {
		return gcm_id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Byte getFoto() {
		return foto;
	}

	public void setFoto(Byte foto) {
		this.foto = foto;
	}

	public String getSobrenome() {
		return sobrenome;
	}

	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}

	public String getSexoChar() {
		return sexo;
	}

	public String getSexo() {
		if (this.sexo.equals("F"))
			return "Feminino";
		else
		return "Masculino";
		
	}

	public void setSexo(String sexo) {
		
		if (sexo.equals("Feminino")){
			this.sexo = "F";
		}else if(sexo.equals("Masculino")){
			this.sexo = "M";
		}else{
			this.sexo = sexo;	
		}
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public CidadeModel getCidade() {
		return cidade;
	}

	public void setCidade(CidadeModel cidade) {
		this.cidade = cidade;
	}

	public String getBloqueio() {
		return bloqueio;
	}

	public void setBloqueio(String bloqueio) {
		this.bloqueio = bloqueio;
	}

}
