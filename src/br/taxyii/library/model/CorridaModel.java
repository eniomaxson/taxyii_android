package br.taxyii.library.model;

import org.json.JSONException;
import org.json.JSONObject;

import br.taxyii.library.utils.HttpClientUtils;

public class CorridaModel extends Model {
	
	private int id;
	private UsuarioModel passageiro;
	private TaxistaModel taxista;
	private HttpClientUtils http;
	private EnderecoModel endereco;
	
	public CorridaModel() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public String toJson() {
		JSONObject json = new JSONObject();
		try {
			json.put("id", (id > 0) ? getId() : "");
			json.put("passageiro_id", passageiro.getId());
			json.put("taxista_id", taxista != null ? taxista.getId(): "");
			json.put("ponto_partida", getEndereco().getCoordenada());
			json.put("ponto_referencia", getEndereco().getPonto_referencia() != null ? getEndereco().getPonto_referencia():"" );
			json.put("numero", getEndereco().getNumero());
			json.put("rua", endereco.getRua() != null ? endereco.getRua(): "");
			json.put("bairro", endereco.getBairro() != null ? endereco.getBairro(): "");
			json.put("cidade", endereco.getCidade() != null ? endereco.getCidade(): "");
			json.put("endereco_favorito_id", endereco.getId() > 0 ? endereco.getId() : "");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return json.toString();
	}
	
	@Override
	public Object toObject(JSONObject jsonObject) {
		
		TaxistaModel taxista = new TaxistaModel();
		CorridaModel corrida = new CorridaModel();
		EnderecoModel endereco = new EnderecoModel();
		UsuarioModel usuario = new UsuarioModel();
		
		try{
			if(jsonObject.has("taxista_id"))
				taxista.setId(jsonObject.getInt("taxista_id"));
			
			if(jsonObject.has("taxista_nome"))
				taxista.setUsuario(jsonObject.getString("taxista_nome"), jsonObject.getString("taxista_gcm_id"));
			
			if(jsonObject.has("passageiro_nome")){
				usuario.setNome(jsonObject.getString("passageiro_nome"));
				usuario.setCelular(jsonObject.getString("passageiro_celular"));
			}
			
			if(jsonObject.has("corrida_id"))
				corrida.setId(jsonObject.getInt("corrida_id"));
			
			if(jsonObject.has("ponto_partida"))
				endereco.setCoordenada(jsonObject.getString("ponto_partida")); 
			
			if(jsonObject.has("rua"))
				endereco.setRua(jsonObject.getString("rua"));
			
			if(jsonObject.has("bairro"))
				endereco.setBairro(jsonObject.getString("bairro"));
			
			if(jsonObject.has("numero"))
				endereco.setNumero(jsonObject.getString("numero"));
			
			if(jsonObject.has("ponto_referencia"))
				endereco.setPonto_referencia(jsonObject.getString("ponto_referencia"));
			
		}catch(JSONException e){
			return null;
		}
		
		if(usuario != null)
			corrida.setPassageiro(usuario);
		if(taxista != null)
			corrida.setTaxista(taxista);
		if(endereco!= null)
			corrida.setEndereco(endereco);
		return corrida;
	}
	
	// ok funcionando
	public String solicitarTaxista() {
		String response = null;
		http = new HttpClientUtils(URL_PREFIX + "corrida/getTaxi");
			
		try {
			response = http.requisicaoPostJson(toJson());
		} catch (Exception e) {
			response = null;
		}
		
		return response;
	}

	/* gets and sets */
	public UsuarioModel getPassageiro() {
		return passageiro;
	}

	public void setPassageiro(UsuarioModel passageiro) {
		this.passageiro = passageiro;
	}

	public TaxistaModel getTaxista() {
		return taxista;
	}

	public void setTaxista(TaxistaModel taxista) {
		this.taxista = taxista;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public EnderecoModel getEndereco() {
		return endereco;
	}

	public void setEndereco(EnderecoModel endereco) {
		this.endereco = endereco;
	}
}
