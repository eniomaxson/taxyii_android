package br.taxyii.library.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import br.taxyii.library.utils.HttpClientUtils;
import br.taxyii.library.utils.SharedPreferencesUtils;

import com.google.android.gms.maps.model.LatLng;

public class TaxistaModel extends Model {

	private int id;
	private String licenca;
	private byte[] documento_frente;
	private byte[] documento_verso;
	private CidadeModel cidade;
	private String cpf;
	private UsuarioModel usuario;
	private String cep;
	private LatLng posicao;
	private HttpClientUtils http;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLicenca() {
		return licenca;
	}

	public void setLicenca(String licenca) {
		this.licenca = licenca;
	}

	public byte[] getDocumento_frente() {
		return documento_frente;
	}

	public void setDocumento_frente(byte[] documento_frente) {
		this.documento_frente = documento_frente;
	}

	public byte[] getDocumento_verso() {
		return documento_verso;
	}

	public void setDocumento_verso(byte[] documento_verso) {
		this.documento_verso = documento_verso;
	}

	public CidadeModel getCidade() {
		return cidade;
	}

	public void setCidade(CidadeModel cidade) {
		this.cidade = cidade;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public UsuarioModel getUsuario() {
		return usuario;
	}

	public void setUsuario(UsuarioModel usuario) {
		this.usuario = usuario;
	}
	
	public UsuarioModel setUsuario(String nome, String gcm_id){
		UsuarioModel usuario = new UsuarioModel();
		usuario.setNome(nome);
		usuario.setGcmId(gcm_id);
		return usuario;
	}
	public LatLng getPosicao() {
		return posicao;
	}

	public void setPosicao(LatLng posicao) {
		this.posicao = posicao;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public TaxistaModel() {
		// TODO Auto-generated constructor stub
	}

	public TaxistaModel(UsuarioModel user) {
		this.usuario = user;
	}

	/*
	 * Metodos de negocio
	 */

	public TaxistaModel(int id) {
		this.id = id;
	}

	/*
	 * Metodos Ijson
	 */

	@Override
	public Object toObject(JSONObject jsonObject) {

		TaxistaModel taxista = new TaxistaModel();
		UsuarioModel usuario = new UsuarioModel();
		try {
			taxista.setId(jsonObject.getInt("id"));
			taxista.setCpf(jsonObject.getString("cpf"));
			taxista.setLicenca(jsonObject.getString("numero_licenca"));
			taxista.setCidade(new CidadeModel(jsonObject.getInt("cidade_id")));
			usuario.setId(jsonObject.getInt("passageiro_id"));
			taxista.setUsuario(usuario);

			return taxista;

		} catch (JSONException e) {
			return null;
		}
	}

	@Override
	public String toJson() {
		JSONObject json = new JSONObject();
		try {
			json.put("passageiro_id", usuario.getId())
					.put("descricao_cidade", cidade.getDescricao())
					.put("cpf", cpf).put("numero_licenca", licenca);
			return json.toString();
		} catch (JSONException e) {
			return null;
		}
	}

	@Override
	public Object toCollectionObject(JSONArray jsonArray) {
		// TODO Auto-generated method stub
		return null;
	}

	public String cadastrar() {
		
		http = new HttpClientUtils(URL_PREFIX + "taxista/create");
		String response;
		try {
			response = http.requisicaoPostJson(toJson());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return response;
	}
	//Metodo responsavel por listar todos os motoristas com sua posição atual
	public String listTaxiPosicao() {
		String url = URL_PREFIX + "taxista/getPosition";
		String response = null;

		http = new HttpClientUtils(url);

		try {
			response = http.requisicaoGetJson();
		} catch (Exception e) {
			return response;
		}

		return response;
	}
	
	public String negarCorrida(CorridaModel corrida, int usuario_id){
		String response = null;
		
		String url = URL_PREFIX + "corrida/negarCorrida?corrida_id=" + corrida.getId() + "&usuario_id=" + usuario_id;
		
		http = new HttpClientUtils(url);
		
		try {
			response = http.requisicaoGetJson();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
	// Metodo responsavel por fazer com que o motorista aceite uma corrida recebendo como parametro o id do usuario do motorista
	
	public String aceitarCorrida(CorridaModel corrida, int usuario_id){
		
		String response = null;
		
		String url = URL_PREFIX + "corrida/aceitarCorrida?corrida_id=" + corrida.getId() + "&usuario_id="+ usuario_id;
		
		http = new HttpClientUtils(url);
		
		try {
			response = http.requisicaoGetJson();
		} catch (Exception e) {
			return null;
		}
		return response;
	}
	
	//Metodo responsavel por setar o taxi para ocupado
	
	public void alterarStatusTaxi(Context ctx, boolean ocupado){
		SharedPreferencesUtils util = new SharedPreferencesUtils(ctx, SharedPreferencesUtils.STATUS_PREFERENCES);
		if(ocupado){
			util.setPreferences(SharedPreferencesUtils.STATUS_TAXI, "O");
		}else{
			util.setPreferences(SharedPreferencesUtils.STATUS_TAXI, "D");
		}
	}
}
