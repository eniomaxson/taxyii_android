package br.taxyii.library.dao;

import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import br.taxyii.library.model.ModeloModel;


public class ModeloDAO {
	private final String NOME_TABELA = "taxi_modelo";

	private static final String sqlDelete = "drop table if exists taxi_modelo";

	private final String[] sqlUpdate  = new String[] {};

	/*
	 * colunas
	 */
	private static String ID = "id";
	private static String NOME = "nome";
	private static String MARCA = "taxi_marca_id";

	private DefaultDao dao;
	private SQLiteDatabase db;

	public ModeloDAO(Context ctx) {
		dao = new DefaultDao(ctx, DefaultDao.NOME_BANCO, DefaultDao.VERSAO,
				DefaultDao.sqlCreate, sqlDelete);
		db = dao.getWritableDatabase();
	}

	public String[] getColunas() {
		String[] colunas = new String[] { ID, NOME, MARCA };
		return colunas;
	}

	public ArrayList<ModeloModel> list() {
		Cursor c = getCursor();
		ArrayList<ModeloModel> modelos = new ArrayList<ModeloModel>();
		if (c != null) {
			while (c.moveToNext()) {
				c.moveToFirst();
				ModeloModel modelo = new ModeloModel();
				modelo.setId(c.getInt(0));
				modelo.setDescricao(c.getString(1));
				modelos.add(modelo);
			}
			fechar();
			return modelos;
		}
		return null;
	}

	public ArrayList<ModeloModel> list(long marca_id) {
		String where = MARCA + "= " + String.valueOf(marca_id);

		Cursor c = db.query(NOME_TABELA, getColunas(), where, null, null, null,
				NOME);

		ArrayList<ModeloModel> modelos = new ArrayList<ModeloModel>();

		if (c != null) {
			while (c.moveToNext()) {
				ModeloModel modelo = new ModeloModel();
				modelo.setId(c.getInt(0));
				modelo.setDescricao(c.getString(1));
				modelos.add(modelo);
			}
			fechar();
			return modelos;
		}
		return null;
	}

	public Cursor getCursor() {
		try {
			return db.query(NOME_TABELA, getColunas(), null, null, null, null,
					null);
		} catch (SQLException e) {
			return null;
		}
	}

	public void fechar() {
		if (db != null) {
			db.close();
			dao.close();
		}
	}
}
