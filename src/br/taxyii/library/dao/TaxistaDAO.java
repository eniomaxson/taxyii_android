package br.taxyii.library.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import br.taxyii.library.model.CidadeModel;
import br.taxyii.library.model.TaxistaModel;


public class TaxistaDAO {

	private final String NOME_TABELA = "taxista";

	private static final String sqlDelete = "drop table if exists taxista";

	private final String[] sqlUpdate  = new String[] { "CREATE TABLE taxista ( "
			+ "id INTEGER PRIMARY KEY, " + "numero_licenca VARCHAR(20), "
			+ "cidade VARCHAR(60), " + "usuario_id INTEGER, "
			+ "cpf CHAR(11), " + "cep VARCHAR(10), " + "documento_verso BLOB, "
			+ "documento_frente BLOB )" };

	/*
	 * colunas
	 */
	private static String ID = "id";
	private static String NUMERO_LICEMCA = "numero_licenca";
	private static String CIDADE = "cidade";
	private static String CPF = "cpf";
	private static String USUARIO = "usuario_id";
	private static String CEP = "cep";
	private static String DOCUMENTO_VERSO = "documento_verso";
	private static String DOCUMENTO_FRENTE = "documento_frente";
	private DefaultDao dao;
	private SQLiteDatabase db;
	private Context ctx;

	public TaxistaDAO(Context ctx) {
		this.ctx = ctx;
		dao = new DefaultDao(ctx, DefaultDao.NOME_BANCO, DefaultDao.VERSAO,
				DefaultDao.sqlCreate, sqlDelete);
		db = dao.getWritableDatabase();
	}

	public String[] getColunas() {
		String[] colunas = new String[] { ID, NUMERO_LICEMCA, CPF, USUARIO,
				CIDADE, CEP, DOCUMENTO_FRENTE, DOCUMENTO_VERSO };
		return colunas;
	}

	public long inserir(TaxistaModel taxista) {
		ContentValues values = new ContentValues();
		values.put(ID, taxista.getId());
		values.put(NUMERO_LICEMCA, taxista.getLicenca());
		values.put(CPF, taxista.getCpf());
		values.put(USUARIO, taxista.getUsuario().getId());
		values.put(CIDADE, taxista.getCidade().getDescricao());
		values.put(CEP, taxista.getCep());
		values.put(DOCUMENTO_VERSO, taxista.getDocumento_verso());
		values.put(DOCUMENTO_FRENTE, taxista.getDocumento_frente());
		long id = inserir(values);
		return id;
	}

	public long inserir(ContentValues valores) {
		long id = db.insert(NOME_TABELA, "", valores);
		fechar();
		return id;
	}

	public int atualizar(TaxistaModel taxista) {
		ContentValues values = new ContentValues();

		values.put(NUMERO_LICEMCA, taxista.getLicenca());
		values.put(CPF, taxista.getCpf());
		values.put(USUARIO, taxista.getUsuario().getId());
		values.put(CIDADE, taxista.getCidade().getDescricao());
		values.put(CEP, taxista.getCep());
		values.put(DOCUMENTO_VERSO, taxista.getDocumento_verso());
		values.put(DOCUMENTO_FRENTE, taxista.getDocumento_frente());
		String _id = String.valueOf(taxista.getId());

		String where = TaxistaDAO.ID + "=?";
		String[] params = new String[] { _id };

		int count = atualizar(values, where, params);

		return count;
	}

	public int atualizar(ContentValues valores, String where, String[] params) {
		int count = db.update(NOME_TABELA, valores, where, params);
		fechar();
		return count;
	}

	public TaxistaModel get() {
		Cursor c = getCursor();
		if (c != null) {
			if (c.getCount() > 0) {
				c.moveToFirst();
				TaxistaModel taxista = new TaxistaModel();

				taxista.setId(c.getInt(0));
				taxista.setLicenca(c.getString(1));
				taxista.setCpf(c.getString(2));
				taxista.setUsuario(new UsuarioDAO(ctx).get());
				taxista.setCidade(new CidadeModel(c.getString(4)));
				taxista.setCep(c.getString(5));
				taxista.setDocumento_frente(c.getBlob(6));
				taxista.setDocumento_verso(c.getBlob(7));
				fechar();
				return taxista;
			}
		}
		return null;
	}

	public Cursor getCursor() {
		try {
			return db.query(NOME_TABELA, getColunas(), null, null, null, null,
					null);
		} catch (SQLException e) {
			return null;
		}
	}

	public int Remover(int id) {
		String _id = String.valueOf(id);
		String where = TaxistaDAO.ID + "= ?";
		String[] params = new String[] { _id };
		return remover(where, params);
	}

	public int remover(String where, String[] params) {
		int qtde = db.delete(NOME_TABELA, where, params);
		fechar();
		return qtde;
	}

	public void fechar() {
		if (db != null) {
			db.close();
			dao.close();
		}
	}

}
