package br.taxyii.library.dao;

import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import br.taxyii.library.model.MarcaModel;


public class MarcaDAO {
	private final String NOME_TABELA = "taxi_marca";

	private static final String sqlDelete = "drop table if exists taxi_marca";

	private final String[] sqlUpdate = new String[] {};
	/*
	 * colunas
	 */
	private static String ID = "id";
	private static String NOME = "nome";
	private DefaultDao dao;
	private SQLiteDatabase db;

	public MarcaDAO(Context ctx) {
		dao = new DefaultDao(ctx, DefaultDao.NOME_BANCO, DefaultDao.VERSAO,
				DefaultDao.sqlCreate, sqlDelete);
		db = dao.getWritableDatabase();
	}

	public String[] getColunas() {
		String[] colunas = new String[] { ID, NOME };
		return colunas;
	}

	public ArrayList<MarcaModel> list() {
		Cursor c = getCursor();
		ArrayList<MarcaModel> marcas = new ArrayList<MarcaModel>();

		if (c != null) {
			while (c.moveToNext()) {
				MarcaModel marca = new MarcaModel();
				marca.setId(c.getInt(0));
				marca.setDescricao(c.getString(1));
				marcas.add(marca);
			}

			fechar();
			return marcas;
		}
		return null;
	}

	public Cursor getCursor() {
		try {
			return db.query(NOME_TABELA, getColunas(), null, null, null, null,
					null);
		} catch (SQLException e) {
			return null;
		}
	}

	public void fechar() {
		if (db != null) {
			db.close();
			dao.close();
		}
	}
}
