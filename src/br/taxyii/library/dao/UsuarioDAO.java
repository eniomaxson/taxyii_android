package br.taxyii.library.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import br.taxyii.library.model.CidadeModel;
import br.taxyii.library.model.UsuarioModel;


public class UsuarioDAO {
	private final String NOME_TABELA = "usuario";
	private static final String sqlDelete = "drop table if exists usuario";
	private static final String[] sqlUpdate = new String[] {};

	/*
	 * colunas
	 */
	private static String ID = "id";
	private static String NOME = "nome";
	private static String SOBRENOME = "sobrenome";
	private static String SEXO = "sexo";
	private static String CELULAR = "celular";
	private static String CIDADE = "cidade";
	private static String EMAIL = "email";
	private static String SENHA = "senha";
	private static String GCM_ID = "gcm_id";
	private static String BLOQUEIO = "bloqueio";
	private DefaultDao dao = null;
	private SQLiteDatabase db;
	private Context context;
	
	public UsuarioDAO(Context ctx) {
		this.context = ctx;
		dao = new DefaultDao(this.context, DefaultDao.NOME_BANCO, DefaultDao.VERSAO,DefaultDao.sqlCreate, sqlDelete);
		db = dao.getWritableDatabase();
	}

	public String[] getColunas() {
		String[] colunas = new String[] { UsuarioDAO.ID, UsuarioDAO.NOME,
				UsuarioDAO.SOBRENOME, UsuarioDAO.SEXO, UsuarioDAO.CELULAR,
				UsuarioDAO.CIDADE, UsuarioDAO.EMAIL, UsuarioDAO.SENHA,
				UsuarioDAO.GCM_ID, UsuarioDAO.BLOQUEIO };
		return colunas;
	}

	public long inserir(UsuarioModel usuario) {
		ContentValues values = new ContentValues();
		values.put(UsuarioDAO.ID, usuario.getId());
		values.put(UsuarioDAO.NOME, usuario.getNome());
		values.put(UsuarioDAO.SOBRENOME, usuario.getSobrenome());
		values.put(UsuarioDAO.SEXO, usuario.getSexo());
		values.put(UsuarioDAO.CELULAR, usuario.getCelular());
		if(usuario.getCidade() != null)
			values.put(UsuarioDAO.CIDADE, usuario.getCidade().getDescricao());
		values.put(UsuarioDAO.EMAIL, usuario.getEmail());
		values.put(UsuarioDAO.SENHA, usuario.getSenha());
		values.put(UsuarioDAO.BLOQUEIO, usuario.getBloqueio());
		long id = inserir(values);
		return id;
	}

	public long inserir(ContentValues valores) {
		long id = db.insert(NOME_TABELA, "", valores);
		fechar();
		return id;
	}

	public int atualizar(UsuarioModel usuario) {
		ContentValues values = new ContentValues();

		values.put(UsuarioDAO.NOME, usuario.getNome());
		values.put(UsuarioDAO.SOBRENOME, usuario.getSobrenome());
		values.put(UsuarioDAO.SEXO, usuario.getSexo());
		values.put(UsuarioDAO.CELULAR, usuario.getCelular());		
		if(usuario.getCidade() != null)
			values.put(UsuarioDAO.CIDADE, usuario.getCidade().getDescricao());	
		values.put(UsuarioDAO.EMAIL, usuario.getEmail());
		values.put(UsuarioDAO.SENHA, usuario.getSenha());
		values.put(UsuarioDAO.GCM_ID, usuario.getGcmId());
		values.put(UsuarioDAO.BLOQUEIO, usuario.getBloqueio());
		
		String _id = String.valueOf(usuario.getId());
		String where = UsuarioDAO.ID + "=?";
		String[] params = new String[] { _id };

		int count = atualizar(values, where, params);
		fechar();
		return count;

	}

	public int atualizar(ContentValues valores, String where, String[] params) {
		int count = db.update(NOME_TABELA, valores, where, params);
		return count;
	}

	public UsuarioModel get() {
		Cursor c = getCursor();
		if (c != null) {
			if (c.getCount() > 0) {
				c.moveToFirst();
				UsuarioModel usuario = new UsuarioModel();
				usuario.setId(c.getInt(0));
				usuario.setNome(c.getString(1));
				usuario.setSobrenome(c.getString(2));
				usuario.setSexo(c.getString(3));
				usuario.setCelular(c.getString(4));
				usuario.setCidade(new CidadeModel(c.getString(5)));
				usuario.setEmail(c.getString(6));
				usuario.setSenha(c.getString(7));
				usuario.setGcmId(c.getString(8));
				usuario.setBloqueio(c.getString(9));
				c.close();
				fechar();
				return usuario;
			}
		}
		return null;
	}

	public Cursor getCursor() {
		try {
			return db.query(NOME_TABELA, getColunas(), null, null, null, null,
					null);
		} catch (SQLException e) {
			return null;
		}
	}

	public int Remover(int id) {
		String _id = String.valueOf(id);
		String where = UsuarioDAO.ID + "= ?";
		String[] params = new String[] { _id };
		return remover(where, params);
	}
		
	public int remover(String where, String[] params) {
		int qtde = db.delete(NOME_TABELA, where, params);
		fechar();
		return qtde;
	}

	public void fechar() {
		if (db != null) {
			db.close();
		}
	}

}
