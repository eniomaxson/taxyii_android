package br.taxyii.library.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DefaultDao extends SQLiteOpenHelper {

	public static String[] sqlCreate = new String[] {

			"CREATE TABLE IF NOT EXISTS usuario(id INTEGER PRIMARY KEY, "
					+ "nome VARCHAR(70), " + "sobrenome VARCHAR(70), "
					+ "sexo VARCHAR(20), " + "email VARCHAR(60), "
					+ "senha VARCHAR(255), " + "celular VARCHAR(20) , "
					+ "gcm_id VARCHAR(255), "
					+ "cidade VARCHAR(80), bloqueio CHAR(1)," + "foto BLOB ) ",
			"CREATE TABLE IF NOT EXISTS taxi_marca ( id INTEGER PRIMARY KEY,nome VARCHAR(20) )",
			"INSERT INTO taxi_marca (id,nome) VALUES (1,'Volkswagen')",
			"INSERT INTO taxi_marca (id,nome) VALUES (2,'Honda')",
			"INSERT INTO taxi_marca (id,nome) VALUES (3,'Hyundai')",
			"INSERT INTO taxi_marca (id,nome) VALUES (4,'Ford')",
			"INSERT INTO taxi_marca (id,nome) VALUES (5,'Chevrolet')",
			"INSERT INTO taxi_marca (id,nome) VALUES (6,'Fiat')",

			"CREATE TABLE IF NOT EXISTS taxi_modelo(id INTEGER PRIMARY KEY,nome VARCHAR(20),taxi_marca_id INTEGER );",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (1,'Amarok',1);",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (2,'Crossfox',1);",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (3,'Fox',1);",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (4,'Fusca',1);",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (5,'Gol',1);",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (6,'Gol G4',1);",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (7,'Golf',1);",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (8,'Jetta',1);",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (9,'Jetta Variant',1);",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (10,'Kombi',1);",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (11,'Parati',1);",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (12,'Passat',1);",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (13,'Passat Variant',1);",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (14,'Polo',1);",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (15,'Polo Sedan',1);",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (16,'Saveiro',1);",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (17,'Space Cross',1);",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (18,'SpaceFox',1);",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (19,'Tiguan',1);",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (20,'Touareg',1);",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (21,'Voyage',1);",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (22,'Accord',2);",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (23,'CR-V',2);",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (24,'City',2);",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (25,'Civic',2);",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (26,'Fit',2);",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (27,'Azera',3);",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (28,'HB20',3);",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (29,'i30',3);",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (30,'HB20S',3);",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (31,'HB20X',3);",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (32,'EcoSport',4);",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (33,'Ka',4);",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (34,'Fiesta Rocam Hatch',4);",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (35,'Fiesta Rocam Sedan',4);",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (36,'Focus Hatch',4);",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (37,'Focus Sedan',4);",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (38,'Astra Sedan',5);",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (39,'Astra Hatch',5);",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (40,'Celta',5);",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (41,'Classic',5);",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (42,'Corsa Hatch',5);",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (43,'Corsa Sedã',5);",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (44,'Vectra',5);",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (45,'Prisma',5);",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (46,'Vectra GT',5);",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (47,'500',6);",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (48,'Bravo',6);",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (49,'500',6);",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (50,'Doblò',6);",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (51,'Grand Siena',6);",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (52,'Idea',6);",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (53,'Bravo',6);",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (54,'Punto',6);",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (55,'Siena EL',6);",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (56,'Uno',6);",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (57,'Palio Weekend',6);",
			"INSERT INTO taxi_modelo (id,nome,taxi_marca_id) VALUES (58,'Palio',6);", };
	private String sqlDelete;
	public static int VERSAO = 1;
	public static final String NOME_BANCO = "taxyii_db";

	public DefaultDao(Context contexto, String nome_banco, int versao,
			String[] sqlCreate, String sqlDelete) {
		super(contexto, nome_banco, null, versao);
		this.sqlDelete = sqlDelete;
		DefaultDao.sqlCreate = sqlCreate;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		int qtdeScript = sqlCreate.length;

		for (int i = 0; i < qtdeScript; i++) {
			String sql = sqlCreate[i];
			db.execSQL(sql);
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int versao_antiga, int versao_nova) {
		db.execSQL(sqlDelete);
		onCreate(db);
	}
}
