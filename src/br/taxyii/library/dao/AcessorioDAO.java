package br.taxyii.library.dao;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import br.taxyii.library.model.AcessorioModel;


public class AcessorioDAO {
	private final String NOME_TABELA = "acessorio";

	private static final String sqlDelete = "drop table if exists acessorio";

	private final String[] sqlCreate = new String[] { "CREATE TABLE acessorio ( "
			+ "id INTEGER PRIMARY KEY, " + "nome VARCHAR(20) )" };

	/*
	 * colunas
	 */
	private static String ID = "id";
	private static String NOME = "nome";
	private DefaultDao dao;
	private SQLiteDatabase db;

	public AcessorioDAO(Context ctx) {
		dao = new DefaultDao(ctx, DefaultDao.NOME_BANCO, DefaultDao.VERSAO, DefaultDao.sqlCreate,sqlDelete);
		db = dao.getWritableDatabase();
	}

	public String[] getColunas() {
		String[] colunas = new String[] { ID, NOME };
		return colunas;
	}

	public long inserir(AcessorioModel acessorio) {
		ContentValues values = new ContentValues();
		values.put(ID, acessorio.getId());
		values.put(NOME, acessorio.getDescricao());
		long id = inserir(values);
		return id;
	}

	public long inserir(ContentValues valores) {
		long id = db.insert(NOME_TABELA, "", valores);
		return id;
	}

	public int atualizar(ContentValues valores, String where, String[] params) {
		int count = db.update(NOME_TABELA, valores, where, params);
		return count;
	}

	public ArrayList<AcessorioModel> list() {
		Cursor c = getCursor();
		ArrayList<AcessorioModel> acessorios = new ArrayList<AcessorioModel>();

		if (c != null) {
			while (c.moveToNext()) {
				AcessorioModel acessorio = new AcessorioModel();
				acessorio.setId(c.getInt(0));
				acessorio.setDescricao(c.getString(1));
				acessorios.add(acessorio);
			}
			fechar();
			return acessorios;
		}
		return null;
	}

	public Cursor getCursor() {
		try {
			return db.query(NOME_TABELA, getColunas(), null, null, null, null,
					null);
		} catch (SQLException e) {
			return null;
		}
	}

	public int Remover(int id) {
		String _id = String.valueOf(id);
		String where = AcessorioDAO.ID + "= ?";
		String[] params = new String[] { _id };
		return remover(where, params);
	}

	public int remover(String where, String[] params) {
		int qtde = db.delete(NOME_TABELA, where, params);
		return qtde;
	}

	public void fechar() {
		if (db != null) {
			db.close();
			dao.close();
		}
	}

}
